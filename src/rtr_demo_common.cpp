#include "rtr_demo.h"

#include "glad/glad.h"
#include "material.h"
#include "engine_ui/material_editor.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "transform.h"
#include "camera.h"
#include "ui/imgui.h"

namespace render_engine
{
    namespace demo_rtr_common
    {
        void set_point_lights(const Material& mat, const glm::vec3* position,
                              const glm::vec3* color, int count)
        {
            /// Set light properties
            auto it = mat.program.light_uniforms.find("l_point_light_pos[0]");
            if(it != mat.program.light_uniforms.end())
                glUniform3fv(it->second.index,count,glm::value_ptr(position[0]));

            auto it_color = mat.program.light_uniforms.find("l_point_light_color[0]");
            if(it_color != mat.program.light_uniforms.end())
                glUniform3fv(it_color->second.index,count,glm::value_ptr(color[0]));
        }

        void set_camera_properties(const Material& mat, const Camera& cam)
        {
            glm::vec3 camera_pos = cam.position;
            /// Set camera properties
            glm::mat4 view_matrix = camera::get_view_matrix(cam);
            glm::mat4 proj_matrix = camera::get_projection_matrix(cam);

            glUniform3fv(mat.program.uniforms[GLProgram::UNIFORM_CAMERA_POSITION],1,glm::value_ptr(camera_pos));
            glUniformMatrix4fv (mat.program.uniforms[GLProgram::UNIFORM_V], 1, GL_FALSE, glm::value_ptr(view_matrix));
            glUniformMatrix4fv (mat.program.uniforms[GLProgram::UNIFORM_P], 1, GL_FALSE, glm::value_ptr(proj_matrix));
        }

        void render_material_editor(const std::string& name,Material& mat)
        {
            material_editor::edit_material(name,mat);
        }

        void get_float3(const glm::vec3& val,float* array)
        {
            array[0] = val.x;
            array[1] = val.y;
            array[2] = val.z;
        }

        void render_transform_ui(TransformInstance& instance)
        {
            ImGui::Begin("Transform");
            float value_holder[3];

            get_float3(instance.transforms[0].position,value_holder);
            ImGui::DragFloat3("Position",value_holder);
            instance.transforms[0].position = glm::vec3(value_holder[0],value_holder[1],value_holder[2]);

            get_float3(instance.transforms[0].rotation,value_holder);
            ImGui::DragFloat3("Rotation",value_holder);
            instance.transforms[0].rotation = glm::vec3(value_holder[0],value_holder[1],value_holder[2]);

            get_float3(instance.transforms[0].scale,value_holder);
            ImGui::DragFloat3("Scale",value_holder);
            instance.transforms[0].scale = glm::vec3(value_holder[0],value_holder[1],value_holder[2]);

            static bool rotate_x,rotate_y,rotate_z;
            ImGui::Checkbox("Rotate X",&rotate_x);
            ImGui::Checkbox("Rotate Y",&rotate_y);
            ImGui::Checkbox("Rotate Z",&rotate_z);
            
            if(rotate_x)
            {
                float rot_x = instance.transforms[0].rotation.x;
                rot_x++;
                instance.transforms[0].rotation = glm::vec3(rot_x,
                                                            instance.transforms[0].rotation.y,
                                                            instance.transforms[0].rotation.z);
            }

            if(rotate_y)
            {
                float rot_y = instance.transforms[0].rotation.y;
                rot_y++;
                instance.transforms[0].rotation = glm::vec3(instance.transforms[0].rotation.x,
                                                            rot_y,
                                                            instance.transforms[0].rotation.z);
            }

            if(rotate_z)
            {
                float rot_z = instance.transforms[0].rotation.z;
                rot_z++;
                instance.transforms[0].rotation = glm::vec3(instance.transforms[0].rotation.x,
                                                            instance.transforms[0].rotation.y,
                                                            rot_z);
            }


            ImGui::End();
        }
    }
}