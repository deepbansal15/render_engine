#ifndef __SCENEGRAPH_EDITOR_H__
#define __SCENEGRAPH_EDITOR_H__

#include "transform.h"
#include "ui/imgui.h"

namespace render_engine
{
    namespace scenegraph_viewer
    {
        static void ShowViewer(TransformInstance& instance)
        {
            ImGui::Begin("Scene");

            struct funcs
            {
                static void ShowNode(TransformInstance& transform_instance, int index)
                {
                    printf("Node view %d %s\n",index,transform_instance.name[index].c_str());
                    ImGui::PushID(index);
                    ImGui::AlignTextToFramePadding();
                    bool node_open = false;
                    if(transform_instance.first_child[index] == -1)
                        node_open = ImGui::TreeNodeEx("",ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_Bullet,
                                                "%s index %u", transform_instance.name[index].c_str(), index);
                    else
                        node_open = ImGui::TreeNode("", "%s index %u", transform_instance.name[index].c_str(), index);
                    
                    if (node_open)
                    {
                        int child = transform_instance.first_child[index];
                        int count = 0;
                        while(child != -1)
                        {
                            //ImGui::PushID(42424242);
                            ShowNode(transform_instance,child);
                            child = transform_instance.next_sibling[child];
                            //ImGui::PopID();
                            count++;
                        }
                        ImGui::TreePop();
                    }
                    ImGui::PopID();
                }
            };

            // Iterate dummy objects with dummy members (all the same data)
            funcs::ShowNode(instance,0);
            
            ImGui::End();
        }


    }
}

#endif