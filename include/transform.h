#ifndef __TRANSFORM_H__
#define __TRANSFORM_H__

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include "mesh.h"

/// Main assumption parent is always before child
/// Need to optimize in terms of memmory layout

namespace render_engine
{
    struct Transform 
    {
        glm::vec3 position;
        glm::vec3 rotation;
        glm::vec3 scale;
        glm::quat quat_rotation;
        glm::mat4 world_matrix;
    };

    struct TransformInstance
    {
        std::vector<Transform> transforms;
        std::vector<int> parent;
        std::vector<int> first_child;
        std::vector<int> next_sibling;
        std::vector<int> mesh_index;
        std::vector<Mesh> meshes;
        std::vector<std::string> name;
    };

    namespace transform_instance
    {
        void update_transform(TransformInstance& instance,bool use_quat);
    }

}

#endif