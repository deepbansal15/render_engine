#ifndef __SCENE_H__
#define __SCENE_H__

#include <vector>
#include <string>
#include "mesh.h"
#include "material.h"

namespace render_engine
{
    struct SceneElement
    {
       Mesh mesh;
       Material mat; 
       std::string mat_name;
       std::string mesh_name;
    };

    struct Scene
    {
        std::vector<SceneElement> elements;
    };
}

#endif