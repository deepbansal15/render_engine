#ifndef __FPS_CAMERA_H__
#define __FPS_CAMERA_H__

namespace render_engine
{
    struct Camera;
    namespace fps_camera
    {
        void keyboard_input(int key,int action,float delta,Camera* camera);
        void mouse_input(double xpos, double ypos,float delta,Camera* camera);
    }
}

#endif