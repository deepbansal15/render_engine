#include "rtr_demo.h"
#include "glad/glad.h"

#include "renderer/shaders.h"
#include "mesh.h"
#include "material.h"
#include "renderer/mesh_primitive.h"
#include "engine_ui/material_editor.h"
#include "skybox.h"
#include "transform.h"

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace render_engine
{
    namespace demo2_rtr
    {
        Mesh sphere_mesh;
        TransformInstance transform_instance;
        Material mat;
        Skybox skybox;

        void init()
        {
            sphere_mesh = mesh_primitive::create_sphere();
            mat = material::create(shaders::reflection_vert,shaders::reflection_frag);
            skybox = skybox::create_skybox(Skybox::CUBEMAP,"../res/skybox/saintlazaruschurch/");

            glprogram::use(mat.program.program);
            auto it = mat.program.custom_uniforms.find("c_u_skybox");
            if(it != mat.program.custom_uniforms.end())
            {
                uint32_t index = it->second.index;
                glUniform1i(index,5);
            }
            glprogram::use(0);
        }

        void render(const Camera& cam)
        {
            glprogram::use(mat.program.program);
            glActiveTexture(GL_TEXTURE5);
            glBindTexture(GL_TEXTURE_CUBE_MAP, skybox.tex.tex_id);
            demo_rtr_common::set_camera_properties(mat,cam);
            
            glm::vec3 sphere_position = glm::vec3(0.0f,0.0f,0.0f);
            glm::mat4 model_matrix = glm::translate(glm::mat4(1),sphere_position);
            glUniformMatrix4fv (mat.program.uniforms[GLProgram::UNIFORM_M], 1, GL_FALSE, glm::value_ptr(model_matrix));
            glBindVertexArray(sphere_mesh.VAO);
            glDrawElements(GL_TRIANGLE_STRIP, sphere_mesh.indices_count, GL_UNSIGNED_INT, 0);
            glprogram::use(0);

            /// Draw skybox
            skybox::render_skybox(skybox,cam);
            glprogram::use(0);
        }

        void render_ui()
        {
            glprogram::use(mat.program.program);
            demo_rtr_common::render_material_editor(std::string("Reflection"),mat);
            glprogram::use(0);
        }

        void shutdown()
        {

        }
    }
}