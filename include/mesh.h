#ifndef __MESH_H__
#define __MESH_H__

#include <inttypes.h>

namespace render_engine
{
    struct Mesh
    {
        static const int VERTEX_STRIDE = 4;
        static const int NORMAL_STRIDE = 3;
        static const int UV_STRIDE = 2;
        static const int POLYGON_VERTEX_COUNT = 3;

        enum
        {
            VERTEX_VBO = 0,
            NORMAL_VBO = 1,
            UV_VBO = 2,
            INDICES_VBO = 3
        };

        int indices_count;

        uint32_t VBO[4];
        uint32_t VAO;
    };

    namespace mesh
    {
        void free_mesh(Mesh* mesh);
    }

}

#endif