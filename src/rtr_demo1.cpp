#include "rtr_demo.h"

#include "glad/glad.h"
#include "renderer/shaders.h"
#include "mesh.h"
#include "material.h"
#include "renderer/mesh_primitive.h"
#include "engine_ui/material_editor.h"

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace render_engine
{
    namespace demo1_rtr
    {
        Mesh sphere_mesh;
        
        Material mat_pbr;
        Material mat_phong;
        Material mat_toon;

        glm::vec3 light_pos[4];
        glm::vec3 light_color[4];

        void init()
        {
            light_pos[0] = glm::vec3(-10.0f,10.0f,10.0f);
            light_pos[1] = glm::vec3(10.0f,10.0f,10.0f);
            light_pos[2] = glm::vec3(-10.0f,-10.0f,10.0f);
            light_pos[3] = glm::vec3(10.0f,-10.0f,10.0f);

            for(int idx=0;idx<4;idx++)
            {
                light_color[idx] = glm::vec3(300.0f,300.0f,300.0f);
            }

            sphere_mesh = mesh_primitive::create_sphere();
            mat_pbr = material::create(shaders::pbr_vert,shaders::pbr_frag);
            mat_phong = material::create(shaders::blinn_phong_vert,shaders::blinn_phong_frag);
            mat_toon = material::create(shaders::toon_vert,shaders::toon_frag);
        }

        void render(const Camera& cam)
        {
            /// Render PBR sphere
            glprogram::use(mat_pbr.program.program);
            demo_rtr_common::set_camera_properties(mat_pbr,cam);
            demo_rtr_common::set_point_lights(mat_pbr,light_pos,light_color,4);
            
            glm::vec3 sphere_position = glm::vec3(-3.5f,0.0f,0.0f);
            glm::mat4 model_matrix = glm::translate(glm::mat4(1),sphere_position);
            glUniformMatrix4fv (mat_pbr.program.uniforms[GLProgram::UNIFORM_M], 1, GL_FALSE, glm::value_ptr(model_matrix));
            glBindVertexArray(sphere_mesh.VAO);
            glDrawElements(GL_TRIANGLE_STRIP, sphere_mesh.indices_count, GL_UNSIGNED_INT, 0);
            glprogram::use(0);

            /// Render blinn-phong sphere
            glprogram::use(mat_phong.program.program);
            demo_rtr_common::set_camera_properties(mat_phong,cam);
            demo_rtr_common::set_point_lights(mat_phong,light_pos,light_color,4);
            
            sphere_position = glm::vec3(0.0f,0.0f,0.0f);
            model_matrix = glm::translate(glm::mat4(1),sphere_position);
            glUniformMatrix4fv (mat_phong.program.uniforms[GLProgram::UNIFORM_M], 1, GL_FALSE, glm::value_ptr(model_matrix));
            glBindVertexArray(sphere_mesh.VAO);
            glDrawElements(GL_TRIANGLE_STRIP, sphere_mesh.indices_count, GL_UNSIGNED_INT, 0);
            glprogram::use(0);

            /// Render toon sphere
            glprogram::use(mat_toon.program.program);
            demo_rtr_common::set_camera_properties(mat_toon,cam);
            demo_rtr_common::set_point_lights(mat_toon,light_pos,light_color,4);
            
            sphere_position = glm::vec3(3.5f,0.0f,0.0f);
            model_matrix = glm::translate(glm::mat4(1),sphere_position);
            glUniformMatrix4fv (mat_toon.program.uniforms[GLProgram::UNIFORM_M], 1, GL_FALSE, glm::value_ptr(model_matrix));
            glBindVertexArray(sphere_mesh.VAO);
            glDrawElements(GL_TRIANGLE_STRIP, sphere_mesh.indices_count, GL_UNSIGNED_INT, 0);
            glprogram::use(0);
        }

        void render_ui()
        {
            glprogram::use(mat_pbr.program.program);
            demo_rtr_common::render_material_editor(std::string("PBR"),mat_pbr);
            glprogram::use(0);

            glprogram::use(mat_phong.program.program);
            demo_rtr_common::render_material_editor(std::string("Blinn-Phong"),mat_phong);
            glprogram::use(0);

            glprogram::use(mat_toon.program.program);
            demo_rtr_common::render_material_editor(std::string("Toon"),mat_toon);
            glprogram::use(0);
        }

        void shutdown()
        {

        }
    }
}