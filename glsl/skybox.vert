in vec4 a_position;

uniform mat4 u_p_matrix;
uniform mat4 u_v_matrix;

out vec3 uv0;

void main()
{
    uv0 = a_position.xyz;
    vec4 pos = u_p_matrix * u_v_matrix * a_position;
    gl_Position = pos.xyww;
}