#ifndef __GLTF_IMPORTER_H__
#define __GLTF_IMPORTER_H__

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <tiny_gltf.h>

namespace render_engine_temp
{
    struct Scene
    {
        std::map<int,uint32_t> gl_buffers;
        std::map<int,Mesh> meshes;
    };

    struct Mesh
    {
        /// Better is to keep VAO same

        struct SubMesh
        {
            uint32_t vao;
            uint32_t mode;
            int component_type;
            size_t count;
            size_t offset;

            int material;
        };

        SubMesh *submeshes;
        uint32_t submesh_count;
    };

    Scene scene;

    int get_attribute_size(int type)
    {
        if(type == TINYGLTF_TYPE_SCALAR)
            return 1;
        else if(type == TINYGLTF_TYPE_VEC2)
            return 2;
        else if(type == TINYGLTF_TYPE_VEC3)
            return 3;
        else if(type == TINYGLTF_TYPE_VEC4)
            return 4;
        else if(type==TINYGLTF_TYPE_MAT2)
            return 4;
        else if(type==TINYGLTF_TYPE_MAT3)
            return 9;
        else if(type==TINYGLTF_TYPE_MAT4)
            return 16;
    }

    int get_attribute_location(const std::string& attrib)
    {
        if(attrib.compare("POSITION") == 0)
            return render_engine::GLProgram::ATTRIB_POSITION;
        else if(attrib.compare("NORMAL") == 0)
            return render_engine::GLProgram::ATTRIB_NORMAL;
        else if(attrib.compare("TANGENT") == 0)
            return render_engine::GLProgram::ATTRIB_TANGENT;
        else if(attrib.compare("TEXCOORD_0") == 0)
            return render_engine::GLProgram::ATTRIB_TEX_COORD;
        else if(attrib.compare("TEXCOORD_1") == 0)
            return render_engine::GLProgram::ATTRIB_TEX_COORD1;
        else if(attrib.compare("COLOR_0") == 0)
            return render_engine::GLProgram::ATTRIB_COLOR;

    }

    GLenum get_primitive_mode(int mode)
    {
        if(mode == TINYGLTF_MODE_TRIANGLES)
            return GL_TRIANGLES;
        else if(mode == TINYGLTF_MODE_TRIANGLE_STRIP)
            return GL_TRIANGLE_STRIP;
        else if(mode == TINYGLTF_MODE_TRIANGLE_FAN)
            return GL_TRIANGLE_FAN;
        else if(mode == TINYGLTF_MODE_POINTS)
            return GL_POINTS;
        else if(mode == TINYGLTF_MODE_LINE)
            return GL_LINES;
        else if(mode == TINYGLTF_MODE_LINE_LOOP)
            return GL_LINE_LOOP;
    }

    void upload_buffer(const tinygltf::Model &model)
    {
        for(int i=0;i<model.bufferViews.size();i++)
        {
            const tinygltf::BufferView &buffer_view = model.bufferViews[i];
            /// Check if buffer view contains mesh or not
            if(buffer_view.target == 0)
                continue;

            const tinygltf::Buffer &buffer = model.buffers[buffer_view.buffer];
            uint32_t buffer; 
            glGenBuffers(1,&buffer);
            glBindBuffer(buffer_view.target,buffer);
            glBufferData(buffer_view.target,
                        buffer_view.byteLength,&buffer.data.at(0) + buffer_view.byteOffset,
                        GL_STATIC_DRAW);
            glBindBuffer(buffer_view.target,0);
            scene.gl_buffers[i] = buffer_state;
        }

        for(int i=0;i<model.meshes.size();i++)
        {
            const tinygltf::Mesh &mesh = model.meshes[i];
            Mesh m;
            m.submesh_count = mesh.primitives.size();
            m.submeshes = new Mesh::SubMesh[m.submesh_count];

            for(int j=0;j<mesh.primitives.size();j++)
            {
                const tinygltf::Primitive &primitive = mesh.primitives[j];
                /// GLDrawArrays not supported
                if(primitives.indices < 0) return;

                std::map<std::string,int>::const_iterator it(primitive.attributes.begin());
                std::map<std::string,int>::const_iterator it_end(primitive.attributes.end());

                glGenVertexArrays(1,&m.submeshes[j].vao);
                glBindVertexArray(m.submeshes[j].vao);

                /// enable vertex atrributes
                for(;it != it_end;it++)
                {
                    const tinygltf::Accessor &accessor = model.accessors[it->second];
                    int size = get_attribute_size(accessor.type);
                    int location = get_attribute_location(it->first);
                    int byte_stride = accessor.ByteStride(model.bufferViews[accessor.bufferView]);
                    int byte_offset = accessor.byteOffset;

                    glBindBuffer(GL_ARRAY_BUFFER,scene.gl_buffers[accessor.bufferView]);
                    glVertexAttribPointer(location,size,accessor.componentType,accessor.normalized ? GL_TRUE : GL_FALSE,
                                        byte_stride,((char*) NULL + byte_offset));
                    glEnableVertexAttribArray(location);
                }

                /// enable indices
                const tinygltf::Accessor &index_accessor = model.accessors[primitive.indices];
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,scene.gl_buffers[index_accessor.bufferView]);

                m.submeshes[j].material = primitive.material;
                m.submeshes[j].mode = get_primitive_mode(primitive.mode);
                m.submeshes[j].component_type = index_accessor.componentType;
                m.submeshes[j].count = index_accessor.count;
                m.submeshes[j].offset = index_accessor.byteOffset;

                glBindVertexArray(0);

            }

            scene.meshes[i] = m;
        }

    }

    void draw_scene()
    {
        std::map<int,Mesh>::const_iterator it(scene.meshes.begin());
        std::map<int,Mesh>::const_iterator it_end(scene.meshes.end());

        for(;it!=it_end;it++)
        {
            const Mesh &mesh = it->second;
            for(int i=0;i<mesh.submesh_count;i++)
            {
                /// Use Material

                glBindVertexArray(mesh.submeshes[i].vao);
                glDrawElements(mesh.submeshes[i].mode,mesh.submeshes[i].count,mesh.submeshes[i].component_type,((char*) NULL + mesh.submeshes[i].offset));
            }
        }
    }

    void LoadtfGL()
    {
        tinygltf::Model model;
        TinyGLTF loader;
        std::string err;
        std::string warn;

        //  bool ret = loader.LoadASCIIFromFile(&model,&err,&warn,"/Users/deepbansal/Work/cg/render_engine/res/buster_drone/scene.gltf");
        //bool ret = loader.LoadASCIIFromFile(&model,&err,&warn,"/Users/deepbansal/Work/cg/render_engine/res/baba_yagas_hut/scene.gltf");
        bool ret = loader.LoadASCIIFromFile(&model,&err,&warn,"/Users/deepbansal/Work/cg/render_engine/res/glTF-Sample-Models-master/2.0/Buggy/gltf/Buggy.gltf");

        if(!warn.empty())
            printf("Warn : %s \n",warn.c_str());

        if(!err.empty())
            printf("Warn : %s \n",err.c_str());

        if(!ret)
            printf("Failed to parse file \n");

        std::cout << "Scene count : " << model.scenes.size() << std::endl;
        print_nodes(model,model.scenes[model.defaultScene > -1 ? model.defaultScene : 0]);
        for(int i=0;i<model.bufferViews.size();i++)
        {
            const tinygltf::BufferView &bufferView = model.bufferViews[i];
            const tinygltf::Buffer &buffer = model.buffers[bufferView.buffer];
            std::cout << "buffer.size= " << buffer.data.size() << ", byteOffset = " << bufferView.byteOffset << std::endl;
        }

    }
}

#endif