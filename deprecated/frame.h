#ifndef __FRAME_H__
#define __FRAME_H__

namespace render_engine
{
    struct Frame
    {
        int current_frame;
        const float delta = 0.016;
    };

    namespace frame
    {
        /// Advance to next frame
        void advance(Frame& frame);
        /// Return time elasped since begining of the simulation
        float simulation_time(Frame& frame);
    }

    namespace frame
    {
        void advance(Frame& frame)          {frame.current_frame++;}
        float simulation_time(Frame& frame)  {return frame.current_frame * frame.delta;}
    }

    struct Array
    {
        void* data;

    };
}

#endif