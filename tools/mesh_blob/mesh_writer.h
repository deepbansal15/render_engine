#ifndef __MESH_WRITER_H__
#define __MESH_WRITER_H__

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

namespace ember_tools
{
    static const char MAGICID[] {'E','M','B','E','R','M','E','S','H'};

    struct float3 {
        float x,y,z; 
        float3(float _x, float _y, float _z):x(_x),y(_y),z(_z){}
    };
    struct float2 {
        float x,y; 
        float2(float _x, float _y):x(_x),y(_y){}
    };
    struct float4 {
        float x,y,z,w; 
        float4(float _x, float _y, float _z, float _w):x(_x),y(_y),z(_z),w(_w){}
    };
    

    struct MeshPart
    {
        uint32_t index_offset;
        uint32_t index_count;

        uint32_t min_index;
        uint32_t max_index;

        uint32_t material;
    };

    struct Node
    {
        int32_t parent;
        int32_t first_child;
        int32_t next_sibling;
        /// Position[3], Rotation[4], Scale[3]
        float transform[10];

        uint32_t part_offset;
        uint32_t parts_count;

        uint32_t name_offset;
        uint32_t name_char_count;
    };

    struct Header
    {
        uint32_t vertex_count;
        uint32_t indices_count;
        uint32_t node_count;
        uint32_t material_count;
        uint32_t parts_count;

        uint32_t position_offset;
        uint32_t normal_offset;
        uint32_t uv0_offset;
        uint32_t uv1_offset;
        uint32_t indices_offset;
        uint32_t nodes_offset;
        uint32_t parts_offset;
        uint32_t material_offset;
        uint32_t node_names_offset;
    };

    struct MeshBlob
    {
        static const int VERTEX_STRIDE = 3;
        static const int NORMAL_STRIDE = 4;
        static const int UV_STRIDE = 2;

        std::vector<float3> position;
        std::vector<float4> normal;
        std::vector<float2> uv0;
        std::vector<float2> uv1;

        std::vector<uint32_t> indices;
        std::vector<std::string> materials;

        std::vector<MeshPart> parts;
        std::vector<Node> nodes;
        std::vector<std::string> nodes_name;
    };
    
    class MeshWriter 
    {
    private:
        uint32_t mFlags;
        void optimize(MeshBlob& mesh);
    public:
        MeshWriter(uint32_t flags) : mFlags(flags) {}
        bool serialize(std::ostream& stream, MeshBlob& mesh);
    };
}

#endif
