#include "camera.h"
#include <stdio.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <glm/ext/quaternion_transform.hpp>

namespace render_engine
{
    namespace camera
    {
        Camera create_perpective(int width, int height, float fov,float near, float far)
        {
            Camera camera;
            camera.position = glm::vec3(0,0,10);
            camera.direction = glm::vec3(0,0,-1);
            camera.up = glm::vec3(0,1.0f,0);

            camera.projection = Camera::Projection::Perspective;
            camera.width = width;
            camera.height = height;

            camera.fov = fov;
            camera.z_near = near;
            camera.z_far = far;

            update_projection_matrix(camera);

            return camera;
        }
        
        Camera create_orthographic(int width, int height, float size, float near, float far)
        {
            Camera camera;
            camera.position = glm::vec3(0,0,10);
            camera.direction = glm::vec3(0,0,-1);
            camera.up = glm::vec3(0,1.0f,0);

            camera.projection = Camera::Projection::Orthographic;
            camera.width = width;
            camera.height = height;

            camera.orthographic_size = size;
            camera.z_near = near;
            camera.z_far = far;

            update_projection_matrix(camera);

            return camera;
        }
        
        glm::mat4 get_view_matrix(const Camera& camera)
        {
            return glm::lookAt(camera.position,camera.position + camera.direction,camera.up);
        }

        glm::mat4 get_projection_matrix(const Camera& camera)
        {
            return camera.projection_matrix;
        }

        glm::mat4 get_view_projection_matrix(const Camera& camera)
        {
            glm::mat4 view = get_view_matrix(camera);
            return camera.projection_matrix * view;
        }

        void update_projection_matrix(Camera& camera)
        {
            if(camera.projection == Camera::Projection::Orthographic)
            {
                float ortho_half_size = camera.orthographic_size * 0.5f;
                float ortho_width = camera.width * ortho_half_size/camera.height;
                float ortho_height = ortho_half_size;
                // Setup orthographic camera
                camera.projection_matrix = glm::ortho(-ortho_width,ortho_width,-ortho_height,ortho_height,camera.z_near,camera.z_far);
            }
            else
            {
                // Setup perspective camera
                camera.projection_matrix = glm::perspective(camera.fov, camera.width/camera.height, camera.z_near, camera.z_far);
            }
        }
    }
}