#include "material.h"

namespace render_engine
{
    namespace material
    {
        Material create(const char* vert_path,const char* frag_path)
        {
            Material mat;
            mat.program = glprogram::create(vert_path,frag_path,true);
            return mat;
        }

        void destroy(Material& mat)
        {
            glprogram::destroy(mat.program);
        }
    }

}