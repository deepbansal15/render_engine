#include "rtr_demo.h"

#include "glad/glad.h"
#include "renderer/shaders.h"
#include "mesh.h"
#include "material.h"
#include "renderer/mesh_primitive.h"
#include "engine_ui/material_editor.h"
#include "skybox.h"
#include "transform.h"
#include "texture.h"
#include "mesh_importer.h"

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace render_engine
{
    namespace demo4_rtr
    {
        TransformInstance transform_instance;
        Texture diffuse_tex;
        Material mat;
        Skybox skybox;
        glm::vec3 light_pos[4];
        glm::vec3 light_color[4];

        void init()
        {
            light_pos[0] = glm::vec3(-10.0f,10.0f,10.0f);
            light_pos[1] = glm::vec3(10.0f,10.0f,10.0f);
            light_pos[2] = glm::vec3(-10.0f,-10.0f,10.0f);
            light_pos[3] = glm::vec3(10.0f,-10.0f,10.0f);

            for(int idx=0;idx<4;idx++)
            {
                light_color[idx] = glm::vec3(300.0f,300.0f,300.0f);
            }

            fbx_loader::import_fbx("../res/rug.fbx",&transform_instance);
            transform_instance.transforms[1].scale = glm::vec3(0.1f,0.1f,0.1f);
            transform_instance.transforms[1].rotation = glm::vec3(-90.0f,0.0f,0.0f);

            mat = material::create(shaders::pbr_vert,shaders::pbr_textured_frag);

            glprogram::use(mat.program.program);
            glUniform1i(mat.program.custom_uniforms["cc_albedo_map"].index,0);
            glUniform1i(mat.program.custom_uniforms["cc_irradiance_map"].index,2);
            glUniform1i(mat.program.custom_uniforms["cc_radiance_map"].index,3);
            glUniform1i(mat.program.custom_uniforms["cc_brdf_lut"].index,4);
            glprogram::use(0);
            
            skybox = skybox::create_skybox(Skybox::HDR,"../res/hdri-skybox/pillars/");

            if(!texture::load_texture("../res/check.tif",&diffuse_tex,true))
                printf("Unable to load texture");
        }
        float rot= 0;
        void render(const Camera& cam)
        {
            glprogram::use(mat.program.program);
            
            demo_rtr_common::set_camera_properties(mat,cam);
            demo_rtr_common::set_point_lights(mat,light_pos,light_color,4);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, diffuse_tex.tex_id);

            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, skybox.hdr_tex.irradiance_tex_id);

            glActiveTexture(GL_TEXTURE3);
            glBindTexture(GL_TEXTURE_2D, skybox.hdr_tex.radiance_tex_id);

            glActiveTexture(GL_TEXTURE4);
            glBindTexture(GL_TEXTURE_2D, skybox.hdr_tex.brdflut_tex_id);

            transform_instance::update_transform(transform_instance,true);
            for(int i=0;i<transform_instance.transforms.size();i++)
            {
                int mesh_index = transform_instance.mesh_index[i];
                if(mesh_index != -1)
                {
                    glm::mat4 model_matrix = transform_instance.transforms[i].world_matrix;
                    glUniformMatrix4fv (mat.program.uniforms[GLProgram::UNIFORM_M], 1, 
                                        GL_FALSE, glm::value_ptr(model_matrix));

                    glBindVertexArray(transform_instance.meshes[mesh_index].VAO);
                    glDrawArrays(GL_TRIANGLES,0,transform_instance.meshes[mesh_index].indices_count);
                    glBindVertexArray(0);
                }
            }

            /// Draw skybox
            skybox::render_skybox(skybox,cam);
            glprogram::use(0);
        }

        void render_ui()
        {
            glprogram::use(mat.program.program);
            demo_rtr_common::render_material_editor(std::string("PBR"),mat);
            glprogram::use(0);

            glprogram::use(skybox.gl_program.program);
            Material m;
            m.program = skybox.gl_program;
            demo_rtr_common::render_material_editor(std::string("Skybox"),m);
            glprogram::use(0);

            demo_rtr_common::render_transform_ui(transform_instance);
        }

        void shutdown()
        {

        }
    }
}