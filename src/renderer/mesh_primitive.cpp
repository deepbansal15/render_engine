#include "renderer/mesh_primitive.h"
#include "glad/glad.h"

#include "mesh.h"
#include "renderer/glprogram.h"

#include <glm/glm.hpp>
#include <vector>

namespace render_engine
{
    namespace mesh_primitive
    {
        Mesh create_cone()
        {

        }

        Mesh create_cube()
        {

        }

        Mesh create_torus()
        {

        }

        Mesh create_sphere()
        {
            Mesh m;
            for(int idx=0;idx<4;idx++)
            {
                m.VBO[idx] = -1;
            }

            glGenVertexArrays(1, &m.VAO);
            glGenBuffers(1, &m.VBO[Mesh::VERTEX_VBO]);
            glGenBuffers(1, &m.VBO[Mesh::INDICES_VBO]);

            std::vector<glm::vec4> positions;
            std::vector<glm::vec2> uv;
            std::vector<glm::vec3> normals;
            std::vector<unsigned int> indices;

            const unsigned int X_SEGMENTS = 64;
            const unsigned int Y_SEGMENTS = 64;
            const float PI = 3.14159265359;
            for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
            {
                for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
                {
                    float xSegment = (float)x / (float)X_SEGMENTS;
                    float ySegment = (float)y / (float)Y_SEGMENTS;
                    float xPos = std::cos(xSegment * 2.0f * PI) * std::sin(ySegment * PI);
                    float yPos = std::cos(ySegment * PI);
                    float zPos = std::sin(xSegment * 2.0f * PI) * std::sin(ySegment * PI);

                    positions.push_back(glm::vec4(xPos, yPos, zPos,1.0f));
                    uv.push_back(glm::vec2(xSegment, ySegment));
                    normals.push_back(glm::vec3(xPos, yPos, zPos));
                }
            }

            bool oddRow = false;
            for (int y = 0; y < Y_SEGMENTS; ++y)
            {
                if (!oddRow) // even rows: y == 0, y == 2; and so on
                {
                    for (int x = 0; x <= X_SEGMENTS; ++x)
                    {
                        indices.push_back(y       * (X_SEGMENTS + 1) + x);
                        indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                    }
                }
                else
                {
                    for (int x = X_SEGMENTS; x >= 0; --x)
                    {
                        indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                        indices.push_back(y       * (X_SEGMENTS + 1) + x);
                    }
                }
                oddRow = !oddRow;
            }
            int index_count = indices.size();
            m.indices_count = index_count;

            std::vector<float> data;
            for (int i = 0; i < positions.size(); ++i)
            {
                data.push_back(positions[i].x);
                data.push_back(positions[i].y);
                data.push_back(positions[i].z);
                data.push_back(positions[i].w);
                if (uv.size() > 0)
                {
                    data.push_back(uv[i].x);
                    data.push_back(uv[i].y);
                }
                if (normals.size() > 0)
                {
                    data.push_back(normals[i].x);
                    data.push_back(normals[i].y);
                    data.push_back(normals[i].z);
                }
            }
            glBindVertexArray(m.VAO);
            glBindBuffer(GL_ARRAY_BUFFER, m.VBO[Mesh::VERTEX_VBO]);
            glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.VBO[Mesh::INDICES_VBO]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
            float stride = (4 + 2 + 3) * sizeof(float);
            glEnableVertexAttribArray(GLProgram::ATTRIB_POSITION);
            glVertexAttribPointer(GLProgram::ATTRIB_POSITION, 4, GL_FLOAT, GL_FALSE, stride, (void*)0);
            glEnableVertexAttribArray(GLProgram::ATTRIB_TEX_COORD);
            glVertexAttribPointer(GLProgram::ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, stride, (void*)(4 * sizeof(float)));
            glEnableVertexAttribArray(GLProgram::ATTRIB_NORMAL);
            glVertexAttribPointer(GLProgram::ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, stride, (void*)(6 * sizeof(float)));

            return m;
        }
    }
}