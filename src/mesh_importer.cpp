#include <stdio.h>
#include <queue>
#include <vector>
#include <iostream>
#include <fstream>

#include "glad/glad.h"
#include "transform.h"
#include "mesh.h"
#include "scene.h"

#include "mesh_importer.h"
#include "renderer/glprogram.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_projection.hpp>
#include <glm/ext/quaternion_common.hpp>

namespace render_engine
{
    namespace fbx_loader
    {
        void import_fbx(const char* fbx_path,TransformInstance* transform_instance)
        {
            // Create FBX Manager
            FbxManager* fbx_manager = FbxManager::Create();

            // IO Settings
            FbxIOSettings* fbx_io_settings = FbxIOSettings::Create(fbx_manager,IOSROOT);
            // Import only model, materials and textures
            fbx_io_settings->SetBoolProp(IMP_FBX_MODEL,true);
            fbx_io_settings->SetBoolProp(IMP_FBX_MATERIAL,true);
            fbx_io_settings->SetBoolProp(IMP_FBX_TEXTURE,true);

            // Disable embedded data
            fbx_io_settings->SetBoolProp(IMP_FBX_EXTRACT_EMBEDDED_DATA,false);
            fbx_manager->SetIOSettings(fbx_io_settings);

            // Initialize the importer
            FbxImporter* fbx_importer = FbxImporter::Create(fbx_manager,"");
            bool status = fbx_importer->Initialize(fbx_path,-1,fbx_manager->GetIOSettings());
            if(!status)
            {
                const char* error_string = fbx_importer->GetStatus().GetErrorString();
                printf("Error importing fbx %s \n",error_string);
                return;
            }

            // Import the scene
            FbxScene* scene = FbxScene::Create(fbx_manager,"myscene");
            fbx_importer->Import(scene);
            FbxGeometryConverter geometry_covertor(fbx_manager);
            geometry_covertor.Triangulate(scene,true);
            // Destroy the importer
            fbx_importer->Destroy();
            // Work on the scene
            FbxNode* root_node = scene->GetRootNode();
            traverse_scene(root_node,transform_instance);

            // Destroy FBX Manager
            fbx_manager->Destroy();
        }

        void traverse_scene(FbxNode* root_node,TransformInstance* transform_instance)
        {
            std::queue<FbxNode*> pending_list;
            
            // Start with root node
            pending_list.push(root_node);
            Transform t;
            get_transform(root_node,&t);
            transform_instance->transforms.push_back(t);
            
            transform_instance->parent.push_back(-1);
            transform_instance->mesh_index.push_back(-1);
            transform_instance->next_sibling.push_back(-1);
            transform_instance->name.push_back(std::string(root_node->GetName()));

            int parent = 0;
            int node_count = 1;
            int mesh_count = 0;

            while(pending_list.size() > 0)
            {
                FbxNode* parent_node = pending_list.front();
                pending_list.pop();
                int child_count = parent_node->GetChildCount();
                transform_instance->first_child.push_back(child_count == 0 ? -1 : node_count);
                
                for(int i=0;i<child_count;i++)
                {
                    FbxNode* child_node = parent_node->GetChild(i);

                    /// Reset pivot
                    FbxGeometry* geom = child_node->GetGeometry();
                    if(geom)
                    {
                        FbxAMatrix iden;
                        iden.SetIdentity();
                        geom->SetPivot(iden);
                        geom->ApplyPivot();
                    }
                    

                    pending_list.push(child_node);
                    FbxNodeAttribute* child_attribute = child_node->GetNodeAttribute();
                    if(child_attribute->GetAttributeType() == FbxNodeAttribute::eMesh)
                    {
                        FbxMesh* fbx_mesh = (FbxMesh*)child_attribute;
                        Mesh mesh;
                        load_mesh(fbx_mesh,&mesh);
                        transform_instance->meshes.push_back(mesh);
                        transform_instance->mesh_index.push_back(mesh_count);
                        mesh_count++;

                        //printf("fbx mesh name %s %d \n",child_node->GetName(),transform_instance->mesh_index.size());
                    }
                    else
                    {
                        // No mesh for this node
                        transform_instance->mesh_index.push_back(-1);
                    }

                    get_transform(child_node,&t);
                    transform_instance->transforms.push_back(t);

                    transform_instance->name.push_back(std::string(child_node->GetName()));
                    transform_instance->parent.push_back(parent);
                    transform_instance->next_sibling.push_back(i+1 < child_count ? node_count+1 : -1);

                    node_count++;
                }

                parent++;
                
            }
            
        for(int i=0;i<transform_instance->parent.size();i++)
        {
            printf("index %d name %s parent %d next %d first %d mesh %d\n",
                  i,transform_instance->name[i].c_str(),transform_instance->parent[i],transform_instance->next_sibling[i],transform_instance->first_child[i]
                  ,transform_instance->mesh_index[i]);
        }
            
        }
        
        void get_transform(FbxNode* node,Transform* transform)
        {
            FbxDouble3 translation = node->LclTranslation.Get();
            FbxDouble3 rotation = node->LclRotation.Get();
            FbxDouble3 scaling = node->LclScaling.Get();
            if(rotation[0] > 0 || rotation[1] > 0 || rotation[2] > 0)
            {
                printf("");
            }
            transform->position = glm::vec3(translation[0],translation[1],translation[2]);
            transform->rotation = glm::radians(glm::vec3(rotation[0],rotation[1],rotation[2]));

            transform->scale = glm::vec3(scaling[0],scaling[1],scaling[2]);
            transform->quat_rotation = glm::angleAxis((float)glm::radians(rotation[2]),glm::vec3(0.0f,0.0f,1.0f)) *
                                       glm::angleAxis((float)glm::radians(rotation[0]),glm::vec3(1.0f,0.0f,0.0f)) *
                                       glm::angleAxis((float)glm::radians(rotation[1]),glm::vec3(0.0f,1.0f,0.0f));
        }

        void load_mesh(FbxMesh* fbx_mesh,Mesh* mesh)
        {
            int polygon_count = fbx_mesh->GetPolygonCount();
            int vertex_count = polygon_count * Mesh::POLYGON_VERTEX_COUNT;
            mesh->indices_count = vertex_count;
            
            float* vertices = new float[vertex_count * Mesh::VERTEX_STRIDE];
            float* normals = new float[vertex_count * Mesh::NORMAL_STRIDE];
            float* uvs = new float[vertex_count * Mesh::UV_STRIDE];

            FbxVector4* control_points = fbx_mesh->GetControlPoints();

            FbxStringList uv_names;
            fbx_mesh->GetUVSetNames(uv_names);
            const char * uv_name = NULL;
            if (uv_names.GetCount())
            {
                uv_name = uv_names[0];
            }

            //printf(" UV count %d normal count %d \n",fbx_mesh->GetElementUVCount(),fbx_mesh->GetElementNormalCount());

            FbxGeometryElementUV* uv_element = fbx_mesh->GetElementUV(uv_name);
            int counter = 0;
            for(int i=0;i<polygon_count;i++)
            {
                for(int j=0;j<Mesh::POLYGON_VERTEX_COUNT;j++)
                {
                    int vertex_idx = fbx_mesh->GetPolygonVertex(i,j);
                    FbxVector4 vertex = control_points[vertex_idx];
                    vertices[counter*Mesh::VERTEX_STRIDE + 0] = static_cast<float>(vertex[0]);
                    vertices[counter*Mesh::VERTEX_STRIDE + 1] = static_cast<float>(vertex[1]);
                    vertices[counter*Mesh::VERTEX_STRIDE + 2] = static_cast<float>(vertex[2]);
                    vertices[counter*Mesh::VERTEX_STRIDE + 3] = 1.0f;


                    FbxVector4 normal;
                    fbx_mesh->GetPolygonVertexNormal(i,j,normal);
                    normals[counter*Mesh::NORMAL_STRIDE + 0] = static_cast<float>(normal[0]);
                    normals[counter*Mesh::NORMAL_STRIDE + 1] = static_cast<float>(normal[1]);
                    normals[counter*Mesh::NORMAL_STRIDE + 2] = static_cast<float>(normal[2]);
        

                    FbxVector2 uv;
                    bool unmapped;
                    fbx_mesh->GetPolygonVertexUV(i,j,uv_name,uv,unmapped);
                    uvs[counter*Mesh::UV_STRIDE + 0] = static_cast<float>(uv[0]);
                    uvs[counter*Mesh::UV_STRIDE + 1] = static_cast<float>(uv[1]);

                    counter++;
                }
            }

            // Upload to the GPU
            glGenBuffers(4,mesh->VBO);
            glGenVertexArrays(1,&mesh->VAO);

            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[0]);
            glBufferData(GL_ARRAY_BUFFER,vertex_count*Mesh::VERTEX_STRIDE * sizeof(float),vertices,GL_STATIC_DRAW);

            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[1]);
            glBufferData(GL_ARRAY_BUFFER,vertex_count * Mesh::NORMAL_STRIDE * sizeof(float),normals,GL_STATIC_DRAW);

            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[2]);
            glBufferData(GL_ARRAY_BUFFER,vertex_count * Mesh::UV_STRIDE * sizeof(float),uvs,GL_STATIC_DRAW);

            glBindVertexArray(mesh->VAO);
    
            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[0]);
            glVertexAttribPointer(render_engine::GLProgram::ATTRIB_POSITION,4,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(render_engine::GLProgram::ATTRIB_POSITION);

            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[1]);
            glVertexAttribPointer(render_engine::GLProgram::ATTRIB_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(render_engine::GLProgram::ATTRIB_NORMAL);
            
            glBindBuffer(GL_ARRAY_BUFFER,mesh->VBO[2]);
            glVertexAttribPointer(render_engine::GLProgram::ATTRIB_TEX_COORD,2,GL_FLOAT,GL_FALSE,0,NULL);
            glEnableVertexAttribArray(render_engine::GLProgram::ATTRIB_TEX_COORD);

            glBindVertexArray(0);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            // printf("");
            delete[] vertices;
            delete[] normals;
            delete[] uvs;
        }
    }

    namespace assimp_loader
    {
        static std::string get_color_string(const char* name,const aiColor4D& color)
        {
            char buffer [128];
            int count = sprintf (buffer, "%s %f %f %f %f \n",name, color.r, color.g, color.b,color.a);
            buffer[count] = '\0';
            return std::string(buffer);
        }

        static std::string get_float_string(const char* name,const float& val)
        {
            char buffer [128];
            int count = sprintf (buffer, "%s %f \n",name, val);
            buffer[count] = '\0';
            return std::string(buffer);
        }

        static std::string get_texture_string(const char* name,int index,const char* val)
        {
            char buffer [1024];
            int count = sprintf (buffer, "%s %d %s \n",name,index, val);
            buffer[count] = '\0';
            return std::string(buffer);
        }

        static void add_texture_type(std::string& mat_string,const aiMaterial* mat,const char* tex_key,aiTextureType type)
        {
            int tex_count = mat->GetTextureCount(type);
            for(int i=0;i<tex_count;i++)
            {
                aiString path;
                mat->GetTexture(type,i,&path);
                mat_string.append(get_texture_string(tex_key,i,path.C_Str()));
            }
        }

        static void write_string_file(const std::string& val,const std::string& path)
        {
            std::ofstream out(path,std::ios::trunc);
            if(!out.good())
            {
                printf("Error while opening file for material");
                out.close();
                return;
            }

            out.write(val.c_str(), val.size());

            out.flush();
            out.close();
        }

        static void process_materials(const aiScene* scene,const std::string& destination)
        {
            /// Write everything to a file

            /// Diffuse color
            /// Diffuse map
            /// Normal map
            /// Displacement map
            /// Specular or Roughness map
            /// Specular value
            /// AO map
            /// Emissive map
            /// emissive color
            /// blend mode (Transparent/Opaque)

            int material_count = scene->mNumMaterials;
            for(int i=0;i<material_count;i++)
            {
                aiMaterial* mat = scene->mMaterials[i];

                std::string mat_string;

                aiString name;
                mat->Get(AI_MATKEY_NAME,name);

                aiColor4D diffuse_color;
                if(mat->Get(AI_MATKEY_COLOR_DIFFUSE,diffuse_color) == AI_SUCCESS)
                {
                    mat_string.append(get_color_string("diffuse_color",diffuse_color));
                }

                float shininess;
                if(mat->Get(AI_MATKEY_SHININESS,shininess) == AI_SUCCESS)
                {
                    mat_string.append(get_float_string("shininess",shininess));
                }

                aiColor4D emissive_color;
                if(mat->Get(AI_MATKEY_COLOR_EMISSIVE,emissive_color) == AI_SUCCESS)
                {
                    mat_string.append(get_color_string("emissive_color",emissive_color));
                }

                float blend;
                if(mat->Get(AI_MATKEY_BLEND_FUNC,blend) == AI_SUCCESS)
                {
                    mat_string.append(get_float_string("blend",blend));
                }

                add_texture_type(mat_string,mat,"diffuse_tex",aiTextureType_DIFFUSE);
                add_texture_type(mat_string,mat,"specular_tex",aiTextureType_SPECULAR);
                add_texture_type(mat_string,mat,"ambient_tex",aiTextureType_AMBIENT);
                add_texture_type(mat_string,mat,"emissive_tex",aiTextureType_EMISSIVE);
                add_texture_type(mat_string,mat,"normal_tex",aiTextureType_NORMALS);
                add_texture_type(mat_string,mat,"shininess_tex",aiTextureType_SHININESS);
                add_texture_type(mat_string,mat,"displacement_tex",aiTextureType_DISPLACEMENT);

                /// Write to a file
                size_t slash = destination.find_last_of("/");
                std::string dir_path = (slash != std::string::npos) ? destination.substr(0, slash) : destination;

                char buffer [1024];
                int count = sprintf (buffer, "%s/%s.mat",dir_path.c_str(),name.C_Str());
                buffer[count] = '\0';
                write_string_file(mat_string,std::string(buffer));
            }
        }
        
        void import_model(const char* path,Scene& out_scene)
        {
            Assimp::Importer importer;
            importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE,
                                        aiPrimitiveType_LINE | aiPrimitiveType_POINT);
            const aiScene* scene = importer.ReadFile(path,aiProcess_Triangulate
                                                          | aiProcess_FlipUVs 
                                                          | aiProcess_CalcTangentSpace
                                                          | aiProcess_GenNormals
                                                          | aiProcess_ImproveCacheLocality
                                                          | aiProcess_JoinIdenticalVertices
                                                          | aiProcess_OptimizeMeshes
                                                          | aiProcess_PreTransformVertices
                                                          | aiProcess_RemoveRedundantMaterials);

            if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
            {
                std::cout << "Cannot import scene" << std::endl;
                return;
            }
            
            std::cout << "Number of mesh : " << scene->mNumMeshes << std::endl; 
            process_materials(scene,std::string(path));
        }
    }
}   