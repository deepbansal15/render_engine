#ifndef __MATERIAL_EDITOR_H__
#define __MATERIAL_EDITOR_H__

#include <string>

namespace render_engine
{
    struct Material;    
    namespace material_editor
    {
        void edit_material(const std::string& mat_name,Material& mat);
    }
}

#endif