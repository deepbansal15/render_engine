#include <getopt.h>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/glm.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "mesh_writer.h"

using namespace Assimp;
using namespace ember_tools;

static void print_scenegraph(const MeshBlob& ember_mesh)
{
    for(int i=0;i<ember_mesh.nodes.size();i++)
    {
        printf("index %d ,name %s parent %d, first_child %d, next_sibling %d \n",i,ember_mesh.nodes_name[i].c_str(),ember_mesh.nodes[i].parent,
        ember_mesh.nodes[i].first_child,ember_mesh.nodes[i].next_sibling);
    }
}

static void get_transform(const aiNode* node,float* transform)
{
    aiVector3D scaling,position;
    aiQuaternion rotation;
    node->mTransformation.Decompose(scaling,rotation,position);

    transform[0] = position.x;
    transform[1] = position.y;
    transform[2] = position.z;

    transform[3] = rotation.x;
    transform[4] = rotation.y;
    transform[5] = rotation.z;
    transform[6] = rotation.w;

    transform[7] = scaling.x;
    transform[8] = scaling.y;
    transform[9] = scaling.z;
}

static std::string get_color_string(const char* name,const aiColor4D& color)
{
    char buffer [128];
    int count = sprintf (buffer, "%s %f %f %f %f \n",name, color.r, color.g, color.b,color.a);
    buffer[count] = '\0';
    return std::string(buffer);
}

static std::string get_float_string(const char* name,const float& val)
{
    char buffer [128];
    int count = sprintf (buffer, "%s %f \n",name, val);
    buffer[count] = '\0';
    return std::string(buffer);
}

static std::string get_texture_string(const char* name,int index,const char* val)
{
    char buffer [1024];
    int count = sprintf (buffer, "%s %d %s \n",name,index, val);
    buffer[count] = '\0';
    return std::string(buffer);
}

static void add_texture_type(std::string& mat_string,const aiMaterial* mat,const char* tex_key,aiTextureType type)
{
    int tex_count = mat->GetTextureCount(type);
    for(int i=0;i<tex_count;i++)
    {
        aiString path;
        mat->GetTexture(type,i,&path);
        mat_string.append(get_texture_string(tex_key,i,path.C_Str()));
    }
}

static void write_string_file(const std::string& val,const std::string& path)
{
    std::ofstream out(path,std::ios::trunc);
    if(!out.good())
    {
        printf("Error while opening file for material");
        out.close();
        return;
    }

    out.write(val.c_str(), val.size());

    out.flush();
    out.close();
}

static void process_materials(const aiScene* scene,const std::string& destination,MeshBlob& ember_mesh)
{
    /// Write everything to a file

    /// Diffuse color
    /// Diffuse map
    /// Normal map
    /// Displacement map
    /// Specular or Roughness map
    /// Specular value
    /// AO map
    /// Emissive map
    /// emissive color
    /// blend mode (Transparent/Opaque)

    int material_count = scene->mNumMaterials;
    for(int i=0;i<material_count;i++)
    {
        aiMaterial* mat = scene->mMaterials[i];

        std::string mat_string;

        aiString name;
        mat->Get(AI_MATKEY_NAME,name);

        /// Push material names to mesh blob
        ember_mesh.materials.push_back(std::string(name.C_Str()));

        aiColor4D diffuse_color;
        if(mat->Get(AI_MATKEY_COLOR_DIFFUSE,diffuse_color) == AI_SUCCESS)
        {
            mat_string.append(get_color_string("diffuse_color",diffuse_color));
        }
        
        float shininess;
        if(mat->Get(AI_MATKEY_SHININESS,shininess) == AI_SUCCESS)
        {
            mat_string.append(get_float_string("shininess",shininess));
        }

        aiColor4D emissive_color;
        if(mat->Get(AI_MATKEY_COLOR_EMISSIVE,emissive_color) == AI_SUCCESS)
        {
            mat_string.append(get_color_string("emissive_color",emissive_color));
        }

        float blend;
        if(mat->Get(AI_MATKEY_BLEND_FUNC,blend) == AI_SUCCESS)
        {
            mat_string.append(get_float_string("blend",blend));
        }

        add_texture_type(mat_string,mat,"diffuse_tex",aiTextureType_DIFFUSE);
        add_texture_type(mat_string,mat,"specular_tex",aiTextureType_SPECULAR);
        add_texture_type(mat_string,mat,"ambient_tex",aiTextureType_AMBIENT);
        add_texture_type(mat_string,mat,"emissive_tex",aiTextureType_EMISSIVE);
        add_texture_type(mat_string,mat,"normal_tex",aiTextureType_NORMALS);
        add_texture_type(mat_string,mat,"shininess_tex",aiTextureType_SHININESS);
        add_texture_type(mat_string,mat,"displacement_tex",aiTextureType_DISPLACEMENT);

        /// Write to a file
        size_t slash = destination.find_last_of("/");
        std::string dir_path = (slash != std::string::npos) ? destination.substr(0, slash) : destination;

        char buffer [1024];
        int count = sprintf (buffer, "%s%s.mat","",name.C_Str());
        buffer[count] = '\0';
        write_string_file(mat_string,std::string(buffer));
    }
}

static float4 calculate_qtangent(const float3& tangent,const float3& bitangent, const float3& normal)
{
    glm::vec3 g_normal(normal.x,normal.y,normal.z);
    glm::vec3 g_tangent(tangent.x,tangent.y,tangent.z);
    glm::vec3 g_bitangent(bitangent.x,bitangent.y,bitangent.z);

    glm::mat3 mat(g_tangent,glm::cross(g_normal,g_tangent),g_normal);
    glm::quat q = glm::quat_cast(mat);

    const float bias = 1.0f / ((1 << (16-1)) - 1.0f);
    if (q.w < bias) {
        q.w = bias;

        const float factor = std::sqrt(1.0 - bias * bias);
        q.x *= factor;
        q.y *= factor;
        q.z *= factor;
    }

    /// If reflection, make w is negative
    if (glm::dot(glm::cross(g_normal,g_tangent),g_bitangent) < 0.0f) {
        q = -q;
    }

    return float4(q.x,q.y,q.z,q.w);
}

static void process_node_mesh(const aiScene* scene,const aiNode* node,MeshBlob& ember_mesh,Node& ember_node)
{
    uint32_t mesh_count = node->mNumMeshes;
    
    if(mesh_count > 0)
        ember_node.part_offset = ember_mesh.parts.size();

    for(uint32_t i=0;i<mesh_count;i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        if (!mesh->HasNormals()) {
            printf("Mesh need to have normals might effect the final look \n");
            continue;
        }

        const float3* vertices = reinterpret_cast<const float3*>(mesh->mVertices);
        const float3* tangents = reinterpret_cast<const float3*>(mesh->mTangents);
        const float3* bitangents = reinterpret_cast<const float3*>(mesh->mBitangents);
        const float3* normals = reinterpret_cast<const float3*>(mesh->mNormals);
        const float4* colors = reinterpret_cast<const float4*>(mesh->mColors[0]);
        const float3* uv0 = reinterpret_cast<const float3*>(mesh->mTextureCoords[0]);
        const float3* uv1 = reinterpret_cast<const float3*>(mesh->mTextureCoords[1]);

        if (!mesh->HasVertexColors(0)) {
            colors = nullptr;
        }
        else
        {
            printf("Mesh colors are not supported \n");
        }

        if (!mesh->HasTextureCoords(0)) {
            uv0 = nullptr;
        }
        if (!mesh->HasTextureCoords(1)) {
            uv1 = nullptr;
        }

        const size_t num_vertices = mesh->mNumVertices;
        if(num_vertices > 0)
        {
            const aiFace* faces = mesh->mFaces;
            const size_t num_faces = mesh->mNumFaces;
            if(num_faces > 0)
            {
                uint32_t indices_offset = ember_mesh.position.size();
                for(size_t j=0;j<num_vertices;j++)
                {
                    ember_mesh.position.push_back(vertices[j]);
                    ember_mesh.uv0.push_back(uv0 ? float2(uv0[j].x,uv0[j].y) : float2(0,0));
                    if(uv1)
                    ember_mesh.uv1.push_back(float2(uv1[j].x,uv1[j].y));
                    ember_mesh.normal.push_back(calculate_qtangent(
                        tangents[j],
                        bitangents[j],
                        normals[j]
                    ));
                }

                uint32_t indices_count = num_faces * faces[0].mNumIndices;
                uint32_t index_buffer_offset = ember_mesh.indices.size();

                for(size_t j =0;j<num_faces;j++)
                {
                    const aiFace& face= faces[j];
                    for (size_t k = 0; k < face.mNumIndices; ++k) {
                        ember_mesh.indices.push_back(uint32_t(face.mIndices[k] + indices_offset));
                    }
                }

                ember_node.parts_count++;
                ember_mesh.parts.push_back(MeshPart {
                    .index_offset = index_buffer_offset,
                    .index_count = indices_count,
                    .material = mesh->mMaterialIndex
                });
            }
        }
    }
}

static void process_scene(const aiScene* scene,MeshBlob& ember_mesh)
{
    std::queue<aiNode*> pending_list;
    uint32_t name_offset = 0;
    // Start with root node
    pending_list.push(scene->mRootNode);

    /// Assign root node
    ember_tools::Node ember_node;
    ember_node.parent = -1;
    ember_node.next_sibling = -1;
    
    ember_node.name_offset = name_offset;
    std::string node_name = std::string(scene->mRootNode->mName.C_Str());
    ember_node.name_char_count = node_name.size();
    name_offset += node_name.size();

    get_transform(scene->mRootNode,ember_node.transform);
    process_node_mesh(scene,scene->mRootNode,ember_mesh,ember_node);

    ember_mesh.nodes.push_back(ember_node);
    ember_mesh.nodes_name.push_back(node_name);


    int parent = 0;
    int node_count = 1;
    int parent_index = 0;

    while(pending_list.size() > 0)
    {
        aiNode* parent_node = pending_list.front();
        pending_list.pop();
        int child_count = parent_node->mNumChildren;
        ember_mesh.nodes[parent].first_child = child_count == 0 ? -1 : node_count;

        for(int i=0;i<child_count;i++)
        {
            
            aiNode* child_node = parent_node->mChildren[i];
            pending_list.push(child_node);
            
            ember_node.parent = parent;
            ember_node.next_sibling = i+1 < child_count ? node_count+1 : -1;
            

            ember_node.name_offset = name_offset;
            std::string node_name = std::string(child_node->mName.C_Str());
            ember_node.name_char_count = node_name.size();
            name_offset += node_name.size();

            get_transform(child_node,ember_node.transform);
            process_node_mesh(scene,child_node,ember_mesh,ember_node);

            ember_mesh.nodes.push_back(ember_node);
            ember_mesh.nodes_name.push_back(node_name);

            node_count++;
        }

        parent++;
        
    }
}

static int handle_arguments(int argc,char* argv[])
{
    static const char* OPTSTR = "hilc";
    static const struct option OPTIONS[] = {
        { "help",        no_argument, 0, 'h' },
        { "license",     no_argument, 0, 'l' },
        { "interleaved", no_argument, 0, 'i' },
        { "compress",    no_argument, 0, 'c' },
        { 0, 0, 0, 0 }  /// termination of the option list
    };
    int opt;
    int option_index = 0;
    while((opt = getopt_long(argc,argv,OPTSTR,OPTIONS,&option_index)) >= 0)
    {
        printf("%c  %d \n",opt,optind);
    }

    return optind;
}

int main(int argc,char* argv[])
{
    int arg_index = handle_arguments(argc,argv);
    
    int num_args = argc - arg_index;
    if (num_args < 2) {
        printf("both paths are required");
        return -1;
    }

    std::string source = std::string(argv[arg_index]);
    std::string destination = std::string(argv[arg_index+1]);
    
    Importer importer;
    importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE,
            aiPrimitiveType_LINE | aiPrimitiveType_POINT);
    importer.SetPropertyBool(AI_CONFIG_IMPORT_COLLADA_IGNORE_UP_DIRECTION, true);
    
    /// Thought about graph optimization but it will chnage the centres of the meshes
    const aiScene* scene = importer.ReadFile(source,
        // normals, tangents
        aiProcess_GenSmoothNormals |
        aiProcess_CalcTangentSpace |
        // topology optimization
        aiProcess_FindInstances |
        aiProcess_OptimizeMeshes |
        aiProcess_JoinIdenticalVertices |
        // misc optimization
        aiProcess_ImproveCacheLocality |
        //aiProcess_PreTransformVertices |
        aiProcess_SortByPType |
        aiProcess_RemoveRedundantMaterials |
        // we only support triangles
        aiProcess_Triangulate
        );
    
    MeshBlob ember_mesh;
    process_scene(scene,ember_mesh);
    process_materials(scene,destination,ember_mesh);
    print_scenegraph(ember_mesh);

    std::ofstream out(destination,std::ios::binary | std::ios::trunc);
    if(!out.good())
    {
        printf("Error while opening the file");
        out.close();
        return -1;
    }

    MeshWriter(0).serialize(out,ember_mesh);

    out.flush();
    out.close();

    return 0;
}