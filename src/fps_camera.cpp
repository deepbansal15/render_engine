#include "camera.h"
#include "fps_camera.h"

#include <stdio.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/ext/quaternion_trigonometric.hpp>
#include <glm/ext/quaternion_transform.hpp>

namespace render_engine
{
    static glm::vec3 camera_forward = glm::vec3(0,0,-1);
    static glm::vec3 camera_up = glm::vec3(0,1,0);
    static float last_x = 640, last_y = 480;
    static float yaw,pitch;

    static bool enable_look = false;
    namespace fps_camera
    {
        void keyboard_input(int key,int action,float delta,Camera* camera)
        {
            if(action == GLFW_PRESS && key == GLFW_KEY_SPACE)
            {
                enable_look = !enable_look;
            }

            float cameraSpeed = 5.0f * delta; // adjust accordingly
            if(action == GLFW_REPEAT)
            {
                if (key == GLFW_KEY_W)
                    camera->position += cameraSpeed * camera_forward;
                if (key == GLFW_KEY_S)
                    camera->position -= cameraSpeed * camera_forward;
                if (key == GLFW_KEY_A)
                    camera->position -= glm::normalize(glm::cross(camera_forward, camera_up)) * cameraSpeed;
                if (key == GLFW_KEY_D)
                    camera->position += glm::normalize(glm::cross(camera_forward, camera_up)) * cameraSpeed;
            }
        }
        
        void mouse_input(double xpos, double ypos,float delta,Camera* camera)
        {
            if(!enable_look)
            {
                last_x = xpos;
                last_y = ypos;
                return;
            }
            float x_offset = xpos - last_x;
            float y_offset = last_y - ypos; // reversed since y-coordinates range from bottom to top
            last_x = xpos;
            last_y = ypos;

            float sensitivity = 1.0f;
            x_offset *= sensitivity;
            y_offset *= sensitivity;

            yaw   += x_offset;
            pitch += y_offset;

            if(pitch > 89.0f)
                pitch =  89.0f;
            if(pitch < -89.0f)
                pitch = -89.0f; 

            glm::vec3 direction;
            direction.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
            direction.y = sin(glm::radians(pitch));
            direction.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
            camera_forward = glm::normalize(direction);

            glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f); 
            glm::vec3 camera_right = glm::normalize(glm::cross(up, camera_forward));
            camera_up = glm::cross(camera_forward, camera_right);

            camera->direction = camera_forward;
            camera->up = camera_up;
            // camera->view_matrix = glm::lookAt(camera->position, camera->position + camera_forward, camera_up);
        }
    }
}