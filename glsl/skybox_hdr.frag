out vec4 frag_colour;

in vec3 uv0;

uniform sampler2D c_u_skybox;
uniform float c_u_exposure;

const vec2 invAtan = vec2(0.1591,0.3183);

vec2 sample_latlong_map(vec3 v)
{
	vec2 uv = vec2(atan(v.z,v.x),asin(v.y));
	uv *= invAtan;
	uv += 0.5;
	return uv;
}

void main()
{
	vec2 uv = sample_latlong_map(normalize(uv0));
	uv = vec2(uv.x,1-uv.y);
	vec3 color = texture(c_u_skybox,uv).rgb;
	color *= c_u_exposure;
	color = pow(color, vec3(1.0/2.2)); 
	frag_colour = vec4(color,1.0);
}