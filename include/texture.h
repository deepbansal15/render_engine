#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <inttypes.h>

struct FIBITMAP;
namespace render_engine
{
    struct Texture
    {
        uint32_t tex_id;
    };

    struct SkyboxTexture
    {
        uint32_t skybox_tex_id;
        uint32_t irradiance_tex_id;
        uint32_t radiance_tex_id;
        uint32_t brdflut_tex_id;
    };

    namespace texture
    {
        bool load_image_data(const char* file_path,uint8_t** data,uint32_t& width,uint32_t& height);
        bool load_texture(const char* file_path,Texture* tex,bool is_mipmap);
        bool load_cubemap(const char* file_path,Texture* tex);
        bool load_equirectangular_skybox(const char* file_path,SkyboxTexture* tex);
    }
}

#endif