#include "glad/glad.h"
#include "engine_ui/material_editor.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>

#include "material.h"
#include "ui/imgui.h"

namespace render_engine
{   
    namespace material_editor
    {
        static void get_property_name(const std::string& in_name,std::string out_name)
        {
            if(in_name.compare("c_u_metallic") == 0)
            {
                out_name += std::string("Metallic");
            }
            else if(in_name.compare("c_u_roughness") == 0)
            {
                out_name += std::string("Roughness");
            }
        }

        void edit_material(const std::string& mat_name,Material& mat)
        {
            ImGui::Begin(mat_name.c_str());
            auto it = mat.program.custom_uniforms.begin();
            auto it_end = mat.program.custom_uniforms.end();
            int size = mat.program.custom_uniforms.size();

            static std::unordered_map<std::string,float> float_val;
            static std::unordered_map<std::string,int> int_val;
            static std::unordered_map<std::string,bool> bool_val;
            static std::unordered_map<std::string,glm::vec3> albedo_val;
            
            for(;it != it_end;it++)
            {
                Uniform uniform = it->second;
                if(uniform.type == GL_FLOAT)
                {
                    auto f_it = float_val.find(it->first);
                    if(f_it == float_val.end())
                        float_val[it->first] = 0.0f;

                    float val = float_val[it->first];
                    float max_range = 1.0f;
                    // if(it->first.compare("c_u_levels") == 0)
                    // {
                    //     max_range = 10.f;
                    // }

                    ImGui::DragFloat(it->first.c_str(),&val,0.01f,0.0f,max_range);
                    glUniform1fv(it->second.index,1,&val);
                    float_val[it->first] = val;
                }
                else if(uniform.type == GL_INT)
                {
                    auto i_it = int_val.find(it->first);
                    if(i_it == int_val.end())
                        int_val[it->first] = 0;

                    int val = int_val[it->first];
                    ImGui::DragInt(it->first.c_str(),&val,1,0,128);
                    glUniform1i(it->second.index,val);
                    int_val[it->first] = val;
                }
                else if(uniform.type == GL_BOOL)
                {
                    auto b_it = bool_val.find(it->first);
                    if(b_it == bool_val.end())
                        bool_val[it->first] = 0;

                    bool val = bool_val[it->first];
                    ImGui::Checkbox(it->first.c_str(),&val);
                    glUniform1i(it->second.index,val);
                    bool_val[it->first] = val;
                }
                else if(uniform.type == GL_FLOAT_VEC3)
                {
                    auto a_it = albedo_val.find(it->first);
                    if(a_it == albedo_val.end())
                        albedo_val[it->first] = glm::vec3(0.0f);
                    
                    glm::vec3 color = albedo_val[it->first];
                    ImGui::ColorPicker3(it->first.c_str(),&color[0],
                                        ImGuiColorEditFlags_NoAlpha | ImGuiColorEditFlags_RGB);
                    glUniform3fv(it->second.index,1,glm::value_ptr(color));
                    albedo_val[it->first] = color;
                }
            }

            ImGui::End();
        }
    }
}