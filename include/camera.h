#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <glm/glm.hpp>
// ?? Why can't I forward declare mat4

namespace render_engine
{
    struct Camera
    {
        enum Projection
        {
            Perspective,
            Orthographic
        };

        glm::vec3 position;
        glm::vec3 direction;
        glm::vec3 up;

        Projection projection;
        
        float width;
        float height;
        
        // Match Height
        float orthographic_size;
        float fov;
        float z_near;
        float z_far;

        glm::mat4 projection_matrix;
    };

    namespace camera
    {
        Camera create_perpective(int width, int height, float fov,float near, float far);
        Camera create_orthographic(int width, int height, float size, float near, float far);
        glm::mat4 get_view_matrix(const Camera& camera);
        glm::mat4 get_projection_matrix(const Camera& camera);
        glm::mat4 get_view_projection_matrix(const Camera& camera);
        void update_projection_matrix(Camera& camera);
    }
}

#endif