#ifndef __IBL_H__
#define __IBL_H__

namespace ember_cubemapgen
{
    class Ibl
    {
    public:
        void generate_irradiance(const float* hdr);
        void generate_radiance(const float* hdr);
    };
}

#endif