out vec4 frag_colour;

uniform vec3 c_u_albedo_p;
uniform int c_u_specular;

vec3 compute_lighting()
{
    vec3 N = normalize(fs_in.normal);
    vec3 V = normalize(u_camera_pos - fs_in.position.xyz);
    
    vec3 ambient = 0.05 * c_u_albedo_p;

    vec3 Lo = vec3(0.0);
    for(int i=0;i<4;i++)
    {
        vec3 L = normalize(l_point_light_pos[i] - fs_in.position.xyz);
        vec3 H = normalize(V + L);
        
        float distance = length(l_point_light_pos[i] - fs_in.position.xyz);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance = l_point_light_color[i] * 0.003 * attenuation;

        vec3 diffuse = c_u_albedo_p * max(dot(L,N),0.0);
        vec3 specular = pow(max(dot(H,N),0.0),c_u_specular) * vec3(0.3);

        Lo += (diffuse + specular);
    }

    return Lo + ambient;
}

void main()
{
	frag_colour = vec4(compute_lighting(),1.0);
}