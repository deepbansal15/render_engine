#ifndef __FBX_IMPORTER_H__
#define __FBX_IMPORTER_H__

#include <fbxsdk.h>

namespace render_engine
{
    struct Mesh;
    struct Transform;
    struct TransformInstance;
    struct Scene;
    namespace fbx_loader
    {
        void import_fbx(const char* fbx_path,TransformInstance* transform_instance);
        void traverse_scene(FbxNode* root_node,TransformInstance* transform_instance);
        void get_transform(FbxNode* node,Transform* transform);
        void load_mesh(FbxMesh* fbx_mesh,Mesh* mesh);
    }

    namespace assimp_loader
    {
        void import_model(const char* path,Scene& out_scene);
    }
}

#endif