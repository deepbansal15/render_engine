#include "skybox.h"

#include "glad/glad.h"
#include "renderer/shaders.h"
#include "camera.h"

#include <glm/gtc/type_ptr.hpp>

namespace render_engine
{
    namespace skybox
    {
        Skybox create_skybox(int type,const char* path)
        {
            Skybox sky_box;
            sky_box.type = type;
            if(type == Skybox::HDR)
                texture::load_equirectangular_skybox(path,&sky_box.hdr_tex);
            else
                texture::load_cubemap(path,&sky_box.tex);

            sky_box.gl_program = glprogram::create(shaders::skybox_vert,
                                                   type == Skybox::HDR ? shaders::skybox_hdr_frag : shaders::skybox_frag,
                                                   false);

            glprogram::use(sky_box.gl_program.program);
            auto it = sky_box.gl_program.custom_uniforms.find("c_u_skybox");
            if(it != sky_box.gl_program.custom_uniforms.end())
            {
                uint32_t index = it->second.index;
                glUniform1i(index,5);
            }

            const float skybox_vertices[] = {
                // positions          
                -1.0f,  1.0f, -1.0f,1.0f,
                -1.0f, -1.0f, -1.0f,1.0f,
                 1.0f, -1.0f, -1.0f,1.0f,
                 1.0f, -1.0f, -1.0f,1.0f,
                 1.0f,  1.0f, -1.0f,1.0f,
                -1.0f,  1.0f, -1.0f,1.0f,

                -1.0f, -1.0f,  1.0f,1.0f,
                -1.0f, -1.0f, -1.0f,1.0f,
                -1.0f,  1.0f, -1.0f,1.0f,
                -1.0f,  1.0f, -1.0f,1.0f,
                -1.0f,  1.0f,  1.0f,1.0f,
                -1.0f, -1.0f,  1.0f,1.0f,

                 1.0f, -1.0f, -1.0f,1.0f,
                 1.0f, -1.0f,  1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                 1.0f,  1.0f, -1.0f,1.0f,
                 1.0f, -1.0f, -1.0f,1.0f,

                -1.0f, -1.0f,  1.0f,1.0f,
                -1.0f,  1.0f,  1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                 1.0f, -1.0f,  1.0f,1.0f,
                -1.0f, -1.0f,  1.0f,1.0f,

                -1.0f,  1.0f, -1.0f,1.0f,
                 1.0f,  1.0f, -1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                 1.0f,  1.0f,  1.0f,1.0f,
                -1.0f,  1.0f,  1.0f,1.0f,
                -1.0f,  1.0f, -1.0f,1.0f,

                -1.0f, -1.0f, -1.0f,1.0f,
                -1.0f, -1.0f,  1.0f,1.0f,
                 1.0f, -1.0f, -1.0f,1.0f,
                 1.0f, -1.0f, -1.0f,1.0f,
                -1.0f, -1.0f,  1.0f,1.0f,
                 1.0f, -1.0f,  1.0f,1.0f
            };

            glGenVertexArrays(1, &sky_box.vao);
            glGenBuffers(1, &sky_box.vbo);
            glBindVertexArray(sky_box.vao);
            glBindBuffer(GL_ARRAY_BUFFER, sky_box.vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(skybox_vertices), &skybox_vertices, GL_STATIC_DRAW);
            glEnableVertexAttribArray(GLProgram::ATTRIB_POSITION);
            glVertexAttribPointer(GLProgram::ATTRIB_POSITION, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
            glBindVertexArray(0);
            glprogram::use(0);

            return sky_box;
        }

        void destroy_skybox(Skybox& skybox)
        {

        }

        void render_skybox(const Skybox& skybox,const Camera& cam)
        {
            glm::mat4 view_matrix = camera::get_view_matrix(cam);
            glm::mat4 proj_matrix = camera::get_projection_matrix(cam);
            
            glDepthFunc(GL_LEQUAL);
            glprogram::use(skybox.gl_program.program);
            glm::mat4 view = glm::mat4(glm::mat3(view_matrix)); // remove translation from the view matrix
            glUniformMatrix4fv (skybox.gl_program.uniforms[render_engine::GLProgram::UNIFORM_V], 1, GL_FALSE, glm::value_ptr(view));
            glUniformMatrix4fv (skybox.gl_program.uniforms[render_engine::GLProgram::UNIFORM_P], 1, GL_FALSE, glm::value_ptr(proj_matrix));
            
            // skybox cube
            glBindVertexArray(skybox.vao);
            glActiveTexture(GL_TEXTURE5);
            glBindTexture(skybox.type ==  Skybox::HDR ? GL_TEXTURE_2D : GL_TEXTURE_CUBE_MAP,
                          skybox.type ==  Skybox::HDR ? skybox.hdr_tex.skybox_tex_id : skybox.tex.tex_id);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);
            glDepthFunc(GL_LESS);
        }
    }
}