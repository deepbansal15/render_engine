#ifndef __SHADERS_H__
#define __SHADERS_H__

namespace render_engine
{
    /// Saving file path
    namespace shaders
    {
        extern const char* base;
        
        extern const char* blinn_phong_vert;
        extern const char* blinn_phong_frag;

        extern const char* toon_vert;
        extern const char* toon_frag;

        extern const char* pbr_vert;
        extern const char* pbr_frag;

        extern const char* pbr_textured_frag;

        extern const char* skybox_vert;
        extern const char* skybox_frag;
        extern const char* skybox_hdr_frag;

        extern const char* reflection_vert;
        extern const char* reflection_frag;

        extern const char* brdf_vert;
        extern const char* brdf_frag;
    }
}


#endif