#ifndef __NEAREST_NEIGHBOR_H__
#define __NEAREST_NEIGHBOR_H__

#include <functional>
#include <vector>
#include <glm/glm.hpp>

namespace render_engine
{
    typedef std::function<void(size_t,const glm::vec3&)> foreachneighbor;
    struct HashGrid
    {
        float spacing;
        glm::vec3 resolution;
        std::vector<glm::vec3> points;
        std::vector<std::vector<int>> buckets;
    }

    namespace hash_grid
    {
        void build(std::vector<glm::vec3>& points,HashGrid& grid);
        int get_hash_key(const glm::vec3& point);
        glm::i32vec3 get_bucket_index(const glm::vec3& position);
        int get_hash_key_bucket_index(const glm::i32vec3& bucket_index);

        void foreach(const HashGrid& grid,const glm::vec3& origin,float radius,const foreachneighbor& callback);
        void get_nearby_keys(const HashGrid& grid,const glm::vec3& position, int* nearby_keys);

    }

    namespace hash_grid
    {
        void build(std::vector<glm::vec3>& points,HashGrid& grid)
        {
            grid.buckets.resize(grid.resolution.x * grid.resolution.y * grid.resolution.z);
            grid.points.resize(points.size());
            int point_count = points.size();

            for(int idx=0;idx<point_count;idx++)
            {
                grid.points[idx] = points[idx];
                int key = get_hash_key(points[idx]);
                grid.buckets[key].push_back(idx);
            }
            
        }

        int get_hash_key(const glm::vec3& point,HashGrid& grid)
        {
            glm::i32vec3 bucket_index = get_bucket_index(point,grid);
            return get_hash_key_bucket_index(bucket_index);
        }

        glm::i32vec3 get_bucket_index(const glm::vec3& position,HashGrid& grid)
        {
            glm::i32vec3 bucket_index;
            bucket_index.x = static_cast<int>(std::floor(position.x/grid.spacing));
            bucket_index.y = static_cast<int>(std::floor(position.y/grid.spacing));
            bucket_index.z = static_cast<int>(std::floor(position.z/grid.spacing));
            return bucket_index;
        }

        int get_hash_key_bucket_index(const glm::i32vec3& bucket_index,HashGrid& grid)
        {
            glm::i32vec3 wrapped_index;
            wrapped_index.x = bucket_index.x % grid.resolution.x;
            wrapped_index.y = bucket_index.y % grid.resolution.y;
            wrapped_index.z = bucket_index.z % grid.resolution.z;

            if(wrapped_index.x < 0) { wrapped_index.x += grid.resolution.x; }
            if(wrapped_index.y < 0) { wrapped_index.y += grid.resolution.y; }
            if(wrapped_index.z < 0) { wrapped_index.z += grid.resolution.z; }

            /// Can be wrong
            return static_cast<int>((wrapped_index.z * grid.resolution.y + wrapped_index.y) *
                                    grid.resolution.x + wrapped_index.x);
        }

        void foreach(const HashGrid& grid,const glm::vec3& origin,float radius,const foreachneighbor& callback)
        {
            if(grid.buckets.empty())
                return;

            int nearby_keys[8];
            get_nearby_keys(grid,origin,nearby_keys);

            // Using diameter is much better to avoid sqrt
            //float diameter = radius;
            for(int idx=0;idx<8;idx++)
            {
                std::vector<int>& bucket = grid.buckets[nearby_keys[idx]];
                int number_points_bucket = bucket.size();

                for(int idy=0;idy<number_points_bucket;idy++)
                {
                    int point_index = bucket[idy];
                    float distance = glm::distance(origin,grid.points[point_index]);
                    if(distance <= diameter)
                    {
                        callback(point_index,grid.points[point_index]);
                    }       
                }
            }
        }

        void get_nearby_keys(const HashGrid& grid,const glm::vec3& position, int* nearby_keys)
        {
            glm::i32vec3 origin_index = get_bucket_index(position);
            glm::i32vec3 nearby_bucket_indices[8];
            
            for(int idx=0;idx<8;idx++)
            {
                nearby_bucket_indices[idx] = origin_index;
            }

            if((origin_index.x + 0.5) * grid.spacing <= position.x)
            {
                nearby_bucket_indices[4].x += 1;
                nearby_bucket_indices[5].x += 1;
                nearby_bucket_indices[6].x += 1;
                nearby_bucket_indices[7].x += 1;
            }
            else
            {
                nearby_bucket_indices[4].x -= 1;
                nearby_bucket_indices[5].x -= 1;
                nearby_bucket_indices[6].x -= 1;
                nearby_bucket_indices[7].x -= 1;
            }

            if((origin_index.y + 0.5) * grid.spacing <= position.y)
            {
                nearby_bucket_indices[2].y += 1;
                nearby_bucket_indices[3].y += 1;
                nearby_bucket_indices[6].y += 1;
                nearby_bucket_indices[7].y += 1;
            }
            else
            {
                nearby_bucket_indices[2].y -= 1;
                nearby_bucket_indices[3].y -= 1;
                nearby_bucket_indices[6].y -= 1;
                nearby_bucket_indices[7].y -= 1;
            }

            if((origin_index.z + 0.5) * grid.spacing <= position.z)
            {
                nearby_bucket_indices[1].z += 1;
                nearby_bucket_indices[3].z += 1;
                nearby_bucket_indices[5].z += 1;
                nearby_bucket_indices[7].z += 1;
            }
            else
            {
                nearby_bucket_indices[1].z -= 1;
                nearby_bucket_indices[3].z -= 1;
                nearby_bucket_indices[5].z -= 1;
                nearby_bucket_indices[7].z -= 1;
            }

            for(int idx=0;idx<8;idx++)
            {
                nearby_keys[idx] = get_hash_key_bucket_index(nearby_bucket_indices[idx]);
            }
        }
    }

    

}

#endif