out vec4 frag_colour;

uniform float c_u_transparency;
uniform samplerCube c_u_skybox;

uniform float EtaR = 0.65;
uniform float EtaG = 0.67; // Ratio of indices of refraction
uniform float EtaB = 0.69;

uniform float Eta;
uniform int c_Dispersion;

void main()
{
    float ER = EtaR;
    float EG = EtaG;
    float EB = EtaB;

    if(c_Dispersion >= 1)
    {
        ER = Eta;
        EG = Eta;
        EB = Eta;
    }

    float FresnelPower = 5.0;
    float F = ((1.0-EG) * (1.0-EG)) / ((1.0+EG) * (1.0+EG));

    vec3 I = normalize(fs_in.position.xyz - u_camera_pos);
    vec3 N = normalize(fs_in.normal);

    float Ratio = F + (1.0 - F) * pow((1.0 - dot(-I, N)), FresnelPower);
    vec3 RefractR = refract(I, N, ER);
    vec3 RefractG = refract(I, N, EG);
    vec3 RefractB = refract(I, N, EB);
    vec3 Reflect = reflect(I, N);
    vec3 refractColor, reflectColor;
    refractColor.r = texture(c_u_skybox, RefractR).r;
    refractColor.g = texture(c_u_skybox, RefractG).g;
    refractColor.b = texture(c_u_skybox, RefractB).b;
    reflectColor = texture(c_u_skybox, Reflect).rgb;

    Ratio = mix(Ratio,1,1-c_u_transparency);
    vec3 color = mix(refractColor, reflectColor, Ratio);
    
    frag_colour = vec4(color,1.0);
}