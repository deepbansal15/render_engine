#ifndef __RTR_DEMO_1_H__
#define __RTR_DEMO_1_H__

#include "glm/glm.hpp"
#include <string>

namespace render_engine
{
    struct Camera;
    struct Material;
    struct TransformInstance;

    namespace demo_rtr_common
    {
        void set_point_lights(const Material& mat, const glm::vec3* position,
                              const glm::vec3* color, int count);
        void set_camera_properties(const Material& mat, const Camera& cam);
        void render_material_editor(const std::string& name,Material& mat);
        void render_transform_ui(TransformInstance& instance);
    }
    namespace demo1_rtr
    {
        void init();
        void render(const Camera& cam);
        void render_ui();
        void shutdown();
    }

    namespace demo2_rtr
    {
        void init();
        void render(const Camera& cam);
        void render_ui();
        void shutdown();
    }

    namespace demo3_rtr
    {
        void init();
        void render(const Camera& cam);
        void render_ui();
        void shutdown();
    }

    namespace demo4_rtr
    {
        void init();
        void render(const Camera& cam);
        void render_ui();
        void shutdown();
    }
}


#endif