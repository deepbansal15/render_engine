in vec4 a_position;
in vec2 a_uv0;

out vec2 uv;

void main()
{
    uv = a_uv0;
	gl_Position = a_position;
}