#include "mesh_writer.h"

#include <meshoptimizer.h>

namespace ember_tools
{
    template<typename T>
    void write(std::ostream& out, const T& value) {
        out.write((const char*) &value, sizeof(T));
    }

    template<typename T>
    void write(std::ostream& out, const T* data, uint32_t count) {
        out.write((const char*) data, sizeof(T) * count);
    }

    void MeshWriter::optimize(MeshBlob& mesh)
    {
        const uint32_t vertex_count = mesh.position.size();
        const uint32_t indices_count = mesh.indices.size();
        /// Optimize vertex cache
        meshopt_optimizeVertexCache(mesh.indices.data(),mesh.indices.data(),indices_count,vertex_count);

        /// Optimize vertex fetch

        // Allocate a remapping table and create a copy of the index buffer.
        std::vector<uint32_t> remapping_vector(vertex_count);
        std::vector<uint32_t> indices_vector = mesh.indices;

        uint32_t* remapping = remapping_vector.data();
        const uint32_t* indices = indices_vector.data();

        // Populate the remapping table.
        meshopt_optimizeVertexFetchRemap(remapping, mesh.indices.data(),
                mesh.indices.size(), vertex_count);

        // Apply the remapping table.
        /// Remap index buffer
        meshopt_remapIndexBuffer(mesh.indices.data(), indices, mesh.indices.size(), remapping);

        /// Remap position buffer
        meshopt_remapVertexBuffer(mesh.position.data(), mesh.position.data(),
                vertex_count, sizeof(float3), remapping);
        
        /// Remap qtangent buffer
        meshopt_remapVertexBuffer(mesh.normal.data(), mesh.normal.data(),
                vertex_count, sizeof(float4), remapping);
        
        /// Remap uv buffer
        meshopt_remapVertexBuffer(mesh.uv0.data(), mesh.uv0.data(),
                vertex_count, sizeof(float2), remapping);
        if (!mesh.uv1.empty()) {
            meshopt_remapVertexBuffer(mesh.uv1.data(), mesh.uv1.data(),
                    vertex_count, sizeof(float2), remapping);
        }

    }

    bool MeshWriter::serialize(std::ostream& stream, MeshBlob& mesh)
    {
        optimize(mesh);
        Header header;
        header.vertex_count = mesh.position.size();
        header.indices_count = mesh.indices.size();
        header.node_count = mesh.nodes.size();
        header.material_count = mesh.materials.size();
        header.parts_count = mesh.parts.size();

        header.position_offset = sizeof(MAGICID) + sizeof(Header);
        header.normal_offset = header.position_offset + header.vertex_count * sizeof(float3);
        header.uv0_offset = header.normal_offset + (header.vertex_count * sizeof(float4));
        header.uv1_offset = mesh.uv1.empty() ? -1 : header.uv0_offset + (header.vertex_count * sizeof(float2));
        header.indices_offset = (header.uv1_offset != -1 ? header.uv1_offset : header.uv0_offset) + 
                                (header.vertex_count * sizeof(float2));
        header.parts_offset = header.indices_offset + header.indices_count * sizeof(uint32_t);
        header.nodes_offset = header.parts_offset + (header.parts_count * sizeof(MeshPart));
        header.material_offset = header.nodes_offset + header.node_count * sizeof(Node);

        void* position_data = mesh.position.data();
        
        write(stream,MAGICID);
        write(stream,header);

        write(stream, mesh.position.data(), uint32_t(mesh.position.size()));
        write(stream, mesh.normal.data(), uint32_t(mesh.normal.size()));
        write(stream, mesh.uv0.data(), uint32_t(mesh.uv0.size()));
        if(!mesh.uv1.empty())
                write(stream, mesh.uv1.data(), uint32_t(mesh.uv1.size()));
        write(stream, mesh.indices.data(), uint32_t(mesh.indices.size()));
        write(stream, mesh.parts.data(), uint32_t(mesh.parts.size()));
        write(stream, mesh.nodes.data(), uint32_t(mesh.nodes.size()));

        uint32_t char_count;
        for(const auto& name : mesh.materials)
        {
           char_count += name.size() + 5;
           write(stream, uint32_t(name.size()));
           write(stream, name.c_str(), uint32_t(name.size()));
           write(stream, char(0));
        }

        header.node_names_offset = header.material_offset + char_count;

        for(const auto& name : mesh.nodes_name)
        {
           write(stream, uint32_t(name.size()));
           write(stream, name.c_str(), uint32_t(name.size()));
           write(stream, char(0));
        }

        return true;
    }
    
}