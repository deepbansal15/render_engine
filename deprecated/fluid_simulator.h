#ifndef __FLUID_SIMULATOR_H__
#define __FLUID_SIMULATOR_H__

#include <vector>
#include <glm/glm.hpp>
#include "frame.h"
#include "collider.h"
#include "vector_field.h"
#include "nearest_neighbor.h"
#include "sph_kernel.h"

/// Parallel for required

namespace render_engine
{
    struct ParticlesData
    {
        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> velocities;
        std::vector<glm::vec3> forces;

        std::vector<glm::vec3> new_positions;
        std::vector<glm::vec3> new_velocities;

        std::vector<std::vector<int>> neighbor_list;
        
        HashGrid grid;
        
        float kernel_radius;
        float radius;
        float inverse_radius;
        float mass;
        float inverse_mass;
    };

    /// Container for all the elements around the fluid
    struct SimulationEnvironment
    {
        const float Wind_Drag_Coefficient = 1e-4;
        const glm::vec3 Gravity(0.0f,-9.8f,0.0f);
        VectorField wind;
        std::vector<Collider> colliders;
    };

    namespace fluid_simulator
    {
        void allocate_particles(ParticlesData& particles_data, int particle_count);
        void setup_environment(SimulationEnvironment& environment);
        

        void begin_advance_timestep(ParticlesData& particles_data);
        void end_advance_timestamp(ParticlesData& particles_data);
        void accumulate_forces(ParticlesData& particles_data);
        void resolve_collision(ParticlesData& particles_data,SimulationEnvironment& environment);
        void time_integration(float delta_time,ParticlesData& particles_data);

        void update(Frame& frame,ParticlesData& particles_data,SimulationEnvironment& environment);

        void build_neighbor_grid(ParticlesData& particles_data,float max_radius);
        void build_neighbor_lists(ParticlesData& particles_data,float max_radius);

        vector<float> densities(const ParticlesData& particles_data);
        glm::vec3 interpolate(const ParticlesData& particles_data,const glm::vec3& origin,const vector<glm::vec3>& values);
    }

    namespace fluid_simulator
    {
        void update(Frame& frame,ParticlesData& particles_data,SimulationEnvironment& environment)
        {
            begin_advance_timestep(particles_data);
            
            accumulate_forces(particles_data);
            delta_time = 0;
            time_integration(delta_time,particles_data);
            resolve_collision(particles_data,environment);

            end_advance_timestamp(particles_data);
        }

        void begin_advance_timestep(ParticlesData& particles_data)
        {
            int particles_size = particles_data.positions.size();
            /// Reset forces 
            for(int idx=0;idx<particles_size;idx++)
            {
                particles_data.forces[idx] = glm::vec3(0);
            }
        }

        void end_advance_timestamp(ParticlesData& particles_data)
        {
            int particles_size = particles_data.positions.size();
            /// Assign new velocities back
            /// Redundant data
            for(int idx=0;idx<particles_size;idx++)
            {
                particles_data.positions[idx] = glm::vec3(0);
                particles_data.velocities[idx] = glm::vec3(0);
            }
        }

        void accumulate_forces(ParticlesData& particles_data,SimulationEnvironment& environment)
        {
            int particles_size = particles_data.positions.size();
            /// Accumulate all the forces (Gravity & drag from air)
            for(int idx=0;idx<particles_size;idx++)
            {
                /// Gravity
                glm::vec3 force = particles_data.mass * environment.Gravity;

                /// Wind
                /// TODO : Calculate wind force

                particles_data.forces[idx] += force;

            }
        }

        void time_integration(float delta_time,ParticlesData& particles_data)
        {
            int particles_size = particles_data.positions.size();
            /// Calculate velocity from force then positions from velocities
            for(int idx=0;idx<particles_size;idx++)
            {
                glm::vec3& new_velocity = particles_data.new_velocities[idx];
                new_velocity = particles_data.velocities[idx] + 
                                delta_time * particles_data.forces[idx] * particles_data.inverse_mass;
                
                glm::vec3& new_position = particles_data.new_positions[idx];
                new_position = particles_data.positions[idx] + new_velocity * delta_time;
            }
        }

        void resolve_collision(ParticlesData& particles_data,SimulationEnvironment& environment)
        {
            /// Resolve collisions and update velocities and positions
        }

        void build_neighbor_grid(ParticlesData& particles_data,float max_radius)
        {
            particles_data.grid.spacing = 2 * max_radius;
            particles_data.grid.resolution = glm::vec3(64);

            hash_grid::build(particles_data.positions,particles_data.grid);
        }

        void build_neighbor_lists(ParticlesData& particles_data,float max_radius)
        {
            int particle_count = particles_data.positions.size();
            particles_data.neighbor_list.resize(particle_count);

            for(int idx=0;idx<particle_count;idx++)
            {
                glm::vec3 origin = particles_data.positions[idx];
                particles_data.neighbor_list[idx].clear();

                hash_grid::foreach(particles_data.grid,origin,max_radius,
                [&](int j,const glm::vec3& ){
                    if(idx != j)
                    {
                        particles_data.neighbor_list[idx].push_back(j);
                    }
                });
            }
        }

        vector<float> densities(const ParticlesData& particles_data)
        {

        }
        
        glm::vec3 interpolate(const ParticlesData& particles_data,const glm::vec3& origin,const vector<glm::vec3>& values)
        {
            glm::vec3 sum;
            vector<float> d = densities(particles_data);
            SphKernel3 kernel = sph_kernel::initialize(particles_data.kernel_radius);

            hash_grid::foreach(particles_data.grid,origin,particles_data.kernel_radius,
            [&](int i,const glm::vec3& neighbor_position){
                float dist = glm::distance(origin,neighbor_position);
                float weight = particles_data.mass / d[i] * sph_kernel::value(kernel,dist);
                sum += weight * values[i];
            });

            return sum;
        }
    }
}

#endif