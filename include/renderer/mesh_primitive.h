#ifndef __MESH_PRIMITVE_H__
#define __MESH_PRIMITVE_H__


namespace render_engine
{
    struct Mesh;
    namespace mesh_primitive
    {
        Mesh create_cone();
        Mesh create_cube();
        Mesh create_torus();
        Mesh create_sphere();
    }
}

#endif