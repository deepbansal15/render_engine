#ifndef __BOUNDING_BOX2_H__
#define __BOUNDING_BOX2_H__

#include <limits>
#include <glm/glm.hpp>

struct BoundingBox2DRayIntersection {
    
    bool is_intersecting = false;
    float near = std::numeric_limits<float>::max();
    float far = std::numeric_limits<float>::max();
};

class BoundingBox2D {
 public:
    glm::vec2 lower_corner;
    glm::vec2 upper_corner;
    BoundingBox2D();
    BoundingBox2D(const glm::vec2& point1, const glm::vec2& point2);

    float width() const;
    float height() const;
    float length(size_t axis);

    bool overlaps(const BoundingBox2D& other) const;
    bool contains(const glm::vec2& point) const;
    bool intersects(const Ray2<T>& ray) const;
    BoundingBox2DRayIntersection closest_intersection(
        const Ray2<T>& ray) const;
    glm::vec2 mid_point() const;
    T diagonal_length() const;
    T diagonal_length_squared() const;
    void reset();
    void merge(const glm::vec2& point);
    void merge(const BoundingBox2D& other);
    void expand(float delta);
    glm::vec2 corner(int idx) const;
    glm::vec2 clamp(const glm::vec2& pt) const;
    bool is_empty() const;
};

#endif
