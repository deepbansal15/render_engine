#ifndef __SHADER_LEGACY_H__
#define __SHADER_LEGACY_H__

#include <inttypes.h>

namespace render_engine
{
    struct ShaderFiles
    {
        static const char* vertex_diffuse;
        static const char* fragment_diffuse;
    };

    struct Shader
    {
        uint32_t shader_program;
    };

    namespace shader
    {
        char* read_shader_source(const char* filepath);
        void compile_shader(uint32_t shader_program,const char* shader_source, int ShaderType);
        void load_shader(Shader* shader);
    }
}

#endif