#ifndef __SPH_KERNEL_H__
#define __SPH_KERNEL_H__

namespace render_engine
{
    struct SphKernel3
    {
        float h, h2, h3;
    }
    namespace sph_kernel
    {
        void initialize(SphKernel3& kernel,float kernel_radius)
        {
            h = kernel_radius;
            h2 = h*h;
            h3 = h2*h;
        }

        float value(SphKernel3& kernel ,float distance)
        {
            if(distance * distance >= kernel.h2)
            {
                return 0.0;
            }
            else
            {
                float x = 1.0f - distance * distance / kernel.h2;
                return 315.0f/(64.0 * 3.14 * kernel.h3) * x * x * x;

            }

        }
    }
}

#endif