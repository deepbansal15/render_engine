#include "renderer/shaders.h"

namespace render_engine
{
    namespace shaders
    {
        const char* base = "../glsl/base.cginc";

        const char* blinn_phong_vert = "../glsl/blinn_phong.vert";
        const char* blinn_phong_frag = "../glsl/blinn_phong.frag";

        const char* toon_vert = "../glsl/toon.vert";
        const char* toon_frag = "../glsl/toon.frag";

        const char* pbr_vert = "../glsl/pbr.vert";
        const char* pbr_frag = "../glsl/pbr.frag";

        const char* pbr_textured_frag = "../glsl/pbr_textured.frag";

        const char* skybox_vert = "../glsl/skybox.vert";
        const char* skybox_frag = "../glsl/skybox.frag";
        const char* skybox_hdr_frag = "../glsl/skybox_hdr.frag";

        const char* reflection_vert = "../glsl/reflection.vert";
        const char* reflection_frag = "../glsl/reflection.frag";

        const char* brdf_vert = "../glsl/brdf.vert";
        const char* brdf_frag = "../glsl/brdf.frag";
    }
}

