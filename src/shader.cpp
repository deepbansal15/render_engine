#include <string> 
#include <fstream>
#include <iostream>
#include <sstream>

#include "glad/glad.h"
#include "shader.h"

namespace render_engine
{
    // Initialize shader files
    const char* ShaderFiles::vertex_diffuse = "../shaders/vertex_diffuse.txt";
    const char* ShaderFiles::fragment_diffuse = "../shaders/fragment_diffuse.txt";
    
    namespace shader
    {
        char* read_shader_source(const char* filepath)
        {
            std::ifstream file(filepath); 
	        if(file.fail()) {
		        std::cout << "error loading shader called " << filepath << std::endl;
		        exit (1); 
	        } 
	
	        std::stringstream stream;
	        stream << file.rdbuf();
	        file.close();

            char* source = new char[stream.str().length() + 1];
            memcpy(source,stream.str().c_str(),stream.str().length());
            source[stream.str().length()] = '\0';

	        return source;
        }

        void compile_shader(uint32_t shader_program,const char* shader_source, int shader_type)
        {
            // create a shader object
            uint32_t shader_obj = glCreateShader(shader_type);
            if (shader_obj == 0) {
                std::cout << "Error creating shader type: \n" << shader_type << std::endl;
                exit(0);
            }

	        glShaderSource(shader_obj, 1, (const GLchar**)&shader_source, NULL);
	        // compile the shader and check for errors
            glCompileShader(shader_obj);
            int success;
	        // check for shader related errors using glGetShaderiv
            glGetShaderiv(shader_obj, GL_COMPILE_STATUS, &success);
            if (!success) {
                GLchar InfoLog[1024];
                glGetShaderInfoLog(shader_obj, 1024, NULL, InfoLog);
                fprintf(stderr, "Error compiling shader type %d: '%s'\n", shader_type, InfoLog);
                exit(1);
            }

	        // Attach the compiled shader object to the program object
            glAttachShader(shader_program, shader_obj);
            glDeleteShader(shader_obj);
        }

        void load_shader(Shader* shader)
        {
            uint32_t shader_program = glCreateProgram();
            if (shader_program == 0) {
                std::cout << "Error creating shader program" << std::endl;
                exit(1);
            }

            char* vertex_code = read_shader_source(ShaderFiles::vertex_diffuse);
            char* fragment_code = read_shader_source(ShaderFiles::fragment_diffuse);

            compile_shader(shader_program,vertex_code,GL_VERTEX_SHADER);
            compile_shader(shader_program,fragment_code,GL_FRAGMENT_SHADER);

            delete[] vertex_code;
            delete[] fragment_code;

            int success = 0;
            char error_log[1024] = { 0 };

            glLinkProgram(shader_program);
            glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
	
            if (success == 0) {
		        glGetProgramInfoLog(shader_program, sizeof(error_log), NULL, error_log);
		        std::cout << "Error linking shader program:\n" <<  error_log << std::endl;
                exit(1);
	        }

            shader->shader_program = shader_program;
        }
    }
}