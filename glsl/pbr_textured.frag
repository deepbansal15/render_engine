/// Reference form Learnopengl.com
out vec4 frag_colour;

// material parameters
uniform sampler2D cc_albedo_map;
uniform sampler2D cc_normal_map;
uniform sampler2D cc_ao_map;

uniform float c_u_metallic;
uniform float c_u_roughness;
const float c_u_ao = 1.0f;

uniform bool c_use_normal_map;
uniform bool c_use_ao_map;

uniform bool c_mipmap_control;
uniform int c_mipmap_level;

// IBL
uniform sampler2D cc_irradiance_map;
uniform sampler2D cc_radiance_map;
uniform sampler2D cc_brdf_lut;

const float PI = 3.14159265359;
const vec2 invAtan = vec2(0.1591,0.3183);

vec2 sample_latlong_map(vec3 v)
{
	vec2 uv = vec2(atan(v.z,v.x),asin(v.y));
	uv *= invAtan;
	uv += 0.5;
	return uv;
}

vec3 get_normal_from_map()
{
    vec3 tangent_normal = texture(cc_normal_map,fs_in.uv0).xyz * 2.0 - 1.0;

    vec3 q1 = dFdx(fs_in.position.xyz);
    vec3 q2 = dFdy(fs_in.position.xyz);
    vec2 st1 = dFdx(fs_in.uv0);
    vec2 st2 = dFdy(fs_in.uv0);

    vec3 N = normalize(fs_in.normal);
    vec3 T = normalize(q1*st2.t - q2*st1.t);
    vec3 B = -normalize(cross(N,T));
    mat3 TBN = mat3(T,B,N);

    return normalize(TBN * tangent_normal);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001); // prevent divide by zero for roughness=0.0 and NdotH=1.0
}


float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}


float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}


vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0,float roughness)
{
    return F0 + (max(vec3(1.0 - roughness),F0) - F0) * pow(1.0-cosTheta,5.0);
}


void main()
{	
    vec3 albedo_tex = vec3(0);
    if(c_mipmap_control)
        albedo_tex = textureLod(cc_albedo_map,fs_in.uv0,c_mipmap_level).rgb;
    else
        albedo_tex = texture(cc_albedo_map,fs_in.uv0).rgb;
    
    vec3 albedo = pow(albedo_tex,vec3(2.2));
    
    /// Optional
    float metallic = c_u_metallic;
    float roughness = c_u_roughness;
    float ao = c_u_ao;

    // float metallic = texture(cc_metallic_map,fs_in.uv0).r;
    // float roughness = texture(cc_roughness_map,fs_in.uv0).r;
    if(c_use_ao_map)
        ao = texture(cc_ao_map,fs_in.uv0).r;


    vec3 N = vec3(0);
    if(c_use_normal_map)
        N = get_normal_from_map();
    else
        N = normalize(fs_in.normal);
    
    vec3 V = normalize(u_camera_pos - fs_in.position.xyz);
    vec3 R = reflect(-V,N);
    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < 4; ++i) 
    {
        // calculate per-light radiance
        vec3 L = normalize(l_point_light_pos[i] - fs_in.position.xyz);
        vec3 H = normalize(V + L);
        float distance = length(l_point_light_pos[i] - fs_in.position.xyz);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance = l_point_light_color[i] * attenuation;

        // Cook-Torrance BRDF
        float NDF = DistributionGGX(N, H, roughness);   
        float G   = GeometrySmith(N, V, L, roughness);      
        vec3 F    = fresnelSchlick(clamp(dot(H, V), 0.0, 1.0), F0);
           
        vec3 nominator    = NDF * G * F; 
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
        vec3 specular = nominator / max(denominator, 0.001); // prevent divide by zero for NdotV=0.0 or NdotL=0.0
        
        // kS is equal to Fresnel
        vec3 kS = F;
        // for energy conservation, the diffuse and specular light can't
        // be above 1.0 (unless the surface emits light); to preserve this
        // relationship the diffuse component (kD) should equal 1.0 - kS.
        vec3 kD = vec3(1.0) - kS;
        // multiply kD by the inverse metalness such that only non-metals 
        // have diffuse lighting, or a linear blend if partly metal (pure metals
        // have no diffuse light).
        kD *= 1.0 - metallic;	  

        // scale light by NdotL
        float NdotL = max(dot(N, L), 0.0);
        // add to outgoing radiance Lo
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;  // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
    }   
    
    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);
    
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - metallic;

    vec2 irradiance_uv = sample_latlong_map(normalize(N));
    vec3 irradiance = texture(cc_irradiance_map, irradiance_uv).rgb;
    vec3 diffuse = albedo * irradiance;

    const float MAX_REFLECTION_LOD = 7.0;
    vec2 radiance_uv = sample_latlong_map(normalize(R));
    vec3 prefilteredColor = textureLod(cc_radiance_map, radiance_uv,  roughness * MAX_REFLECTION_LOD).rgb;    
    vec2 brdf  = texture(cc_brdf_lut, vec2(max(dot(N, V), 0.0), roughness)).rg;
    vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);

    vec3 ambient = (kD * diffuse + specular) * ao;
    vec3 color = ambient + Lo;
    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2)); 

    frag_colour = vec4(color, 1.0);
}