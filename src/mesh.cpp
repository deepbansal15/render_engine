#include "mesh.h"
#include "glad/glad.h"

namespace render_engine
{
    namespace mesh
    {
        void free_mesh(Mesh* mesh)
        {
            glDeleteBuffers(4,mesh->VBO);
            glDeleteVertexArrays(1,&mesh->VAO);
        }
    }
}