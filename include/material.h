#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include "renderer/glprogram.h"

namespace render_engine
{
    struct Material
    {
        GLProgram program;
    };

    namespace material
    {
        void save(const Material& mat);
        Material load(GLProgram* program,const char* mat_path);
        Material create(const char* vert_path,const char* frag_path);
        void destroy(Material& mat);
    }

}

#endif