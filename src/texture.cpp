#include <FreeImage.h>
#include "glad/glad.h"

#include "texture.h"
#include <string>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include "lut_generator.h"

namespace render_engine
{
    namespace texture
    {
		bool load_image_data(const char* file_path,uint8_t** data,uint32_t& width,uint32_t& height)
		{
			//image format
	        FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	        //pointer to the image, once loaded
	        FIBITMAP *dib(0);
	        //pointer to the image data
			uint8_t* bits(0);
	        //image width and height
	        
	
	        //check the file signature and deduce its format
	        fif = FreeImage_GetFileType(file_path, 0);
	        //if still unknown, try to guess the file format from the file extension
	        if(fif == FIF_UNKNOWN) 
		        fif = FreeImage_GetFIFFromFilename(file_path);
	        //if still unkown, return failure
	        if(fif == FIF_UNKNOWN)
		        return false;
			
			

	        //check that the plugin has reading capabilities and load the file
	        if(FreeImage_FIFSupportsReading(fif))
		        dib = FreeImage_Load(fif, file_path);
	        //if the image failed to load, return failure
	        if(!dib)
		        return false;
			
			FreeImage_FlipVertical(dib);

			//retrieve the image data
	        bits = FreeImage_GetBits(dib);
	        //get the image width and height
	        width = FreeImage_GetWidth(dib);
	        height = FreeImage_GetHeight(dib);
			int bpp = FreeImage_GetBPP(dib);
			bpp /= 8;

			*data = (uint8_t*)malloc(width*height*bpp);
			memcpy(*data,bits,width*height*bpp);

			//Free FreeImage's copy of the data
	        FreeImage_Unload(dib);

			return true;
		}

        bool load_texture(const char* file_path,Texture* tex,bool is_mipmap)
        {
			uint8_t* bits(0);
			uint32_t width(0), height(0);

			if(!load_image_data(file_path,&bits,width,height))
			{
				return false;
			}

	        //if this somehow one of these failed (they shouldn't), return failure
	        if((bits == 0) || (width == 0) || (height == 0))
		        return false;

	        //generate an OpenGL texture ID for this texture
			uint32_t tex_id;
	        glGenTextures(1, &tex_id);
			glBindTexture(GL_TEXTURE_2D, tex_id);
	        //bind to the new texture ID
			if(is_mipmap)
			{
				//glTexStorage2D(GL_TEXTURE_2D, 7, GL_RGB, width, height);
				//glTexSubImage2D(GL_TEXTURE_2D,0,0,0,width, height, GL_BGR, GL_UNSIGNED_BYTE, bits);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
		        			 0, GL_BGR, GL_UNSIGNED_BYTE, bits);
				glGenerateMipmap(GL_TEXTURE_2D);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			}
			else
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
	        	//store the texture data for OpenGL use
	        	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
		        			 0, GL_BGR, GL_UNSIGNED_BYTE, bits);
			}

			free(bits);

			glBindTexture(GL_TEXTURE_2D, 0);
			tex->tex_id = tex_id;
	        
	        //return success
	        return true;
        }

		bool load_cubemap(const char* file_path,Texture* tex)
		{
			const int cube_faces = 6;
			std::string faces_path[] = {
				std::string(file_path) + std::string("posx.jpg"),
				std::string(file_path) + std::string("negx.jpg"),
				std::string(file_path) + std::string("posy.jpg"),
				std::string(file_path) + std::string("negy.jpg"),
				std::string(file_path) + std::string("posz.jpg"),
				std::string(file_path) + std::string("negz.jpg")
			};

			uint32_t tex_id;
			glGenTextures(1, &tex_id);
			glBindTexture(GL_TEXTURE_CUBE_MAP, tex_id);

			for(int idx=0;idx<cube_faces;idx++)
			{
				uint8_t* bits(0);
				uint32_t width(0), height(0);

				if(!load_image_data(faces_path[idx].c_str(),&bits,width,height))
				{
					return false;
				}
				
	        	//if this somehow one of these failed (they shouldn't), return failure
	        	if((bits == 0) || (width == 0) || (height == 0))
		    	    return false;
	        	
	        	//store the texture data for OpenGL use
	        	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + idx, 0, GL_RGB, width, height,
		    	    0, GL_BGR, GL_UNSIGNED_BYTE, bits);
				
				free(bits);
			}

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE);

			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

			tex->tex_id = tex_id;
			return true;
		}

		bool load_equirectangular_skybox(const char* file_path,SkyboxTexture* tex)
		{
			const int cube_faces = 6;
			std::string skybox_path = std::string(file_path) + std::string("pillars_2k.hdr");

			/// Load skybox texture

			uint32_t tex_id;
			glGenTextures(1, &tex_id);
			glBindTexture(GL_TEXTURE_2D, tex_id);
			uint8_t* bits(0);
			uint32_t width(0), height(0);
			if(!load_image_data(skybox_path.c_str(),&bits,width,height))
			{
				return false;
			}
				
	        //if this somehow one of these failed (they shouldn't), return failure
	        if((bits == 0) || (width == 0) || (height == 0))
		        return false;
	        
	        //store the texture data for OpenGL use
	        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height,
		        0, GL_RGB, GL_FLOAT, bits);
			
			free(bits);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

			glBindTexture(GL_TEXTURE_2D, 0);

			tex->skybox_tex_id = tex_id;

			/// Load illuminance

			skybox_path = std::string(file_path) + std::string("irradiance.tga");
			uint32_t irradiance_tex_id;
			glGenTextures(1, &irradiance_tex_id);
			glBindTexture(GL_TEXTURE_2D, irradiance_tex_id);
			bits = 0;
			width = 0;
			height = 0;
			if(!load_image_data(skybox_path.c_str(),&bits,width,height))
			{
				return false;
			}
				
	        //if this somehow one of these failed (they shouldn't), return failure
	        if((bits == 0) || (width == 0) || (height == 0))
		        return false;
	        
	        //store the texture data for OpenGL use
	        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
		        0, GL_BGR, GL_UNSIGNED_BYTE, bits);
			
			free(bits);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

			glBindTexture(GL_TEXTURE_2D, 0);

			tex->irradiance_tex_id = irradiance_tex_id;

			/// Load radiance
			const int mipmap_level = 7;
			uint32_t radiance_tex_id;
			glGenTextures(1, &radiance_tex_id);
			glBindTexture(GL_TEXTURE_2D, radiance_tex_id);

			for(int idx=0;idx<mipmap_level;idx++)
			{
				std::string filename = std::string("radiance_") + std::to_string(idx) + std::string(".tga");
				skybox_path = std::string(file_path) + filename;

				bits = 0;
				width = 0;
				height = 0;
				if(!load_image_data(skybox_path.c_str(),&bits,width,height))
				{
					return false;
				}

	        	//if this somehow one of these failed (they shouldn't), return failure
	        	if((bits == 0) || (width == 0) || (height == 0))
		    	    return false;

	        	//store the texture data for OpenGL use
	        	glTexImage2D(GL_TEXTURE_2D, idx, GL_RGB, width, height,
		    	    0, GL_BGR, GL_UNSIGNED_BYTE, bits);

				free(bits);
			}

			glGenerateMipmap(GL_TEXTURE_2D);
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D, 0);
			tex->radiance_tex_id = radiance_tex_id;

			/// Load LUT
			lut_generator::generate_lut(tex->brdflut_tex_id);

			return true;
		}
    }
}