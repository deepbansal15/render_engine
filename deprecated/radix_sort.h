#ifndef __RADIX_SORT_H__
#define __RADIX_SORT_H__

#include <stdlib.h>
#include <vector>

namespace render_engine
{
    struct Data
    {
        std::vector<int> array;
    };
    namespace radix_sort
    {
        void initialize(Data& data);
        void sort(Data& data);
    }

    namespace radix_sort
    {
        void initialize(Data& data)
        {
            for(int idx=0;idx<100;idx++)
            {
                data.array.push_back(rand()%100+1);
            }
        }

        void sort(Data& data)
        {   

        }   
    }
}

#endif