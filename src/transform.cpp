#include "transform.h"
#include "mesh.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/ext/quaternion_trigonometric.hpp>


namespace render_engine
{
    namespace transform_instance
    {
        void update_transform(TransformInstance& instance,bool use_quat)
        {
            for(int i=0;i<instance.parent.size();i++)
            {
                glm::mat4 parent = instance.parent[i] == -1 ? glm::mat4(1) : instance.transforms[instance.parent[i]].world_matrix;
                glm::mat4 local(1);
			    local = glm::translate(local, instance.transforms[i].position);
			    
                if(use_quat)
                {
                    instance.transforms[i].quat_rotation = glm::angleAxis((float)glm::radians(instance.transforms[i].rotation.z),glm::vec3(0.0f,0.0f,1.0f)) *
                                       glm::angleAxis((float)glm::radians(instance.transforms[i].rotation.x),glm::vec3(1.0f,0.0f,0.0f)) *
                                       glm::angleAxis((float)glm::radians(instance.transforms[i].rotation.y),glm::vec3(0.0f,1.0f,0.0f));

                    local *= glm::toMat4(instance.transforms[i].quat_rotation);
                }
                else
                {
                    local *= glm::toMat4(glm::quat(glm::radians(instance.transforms[i].rotation)));
                }

			    local = glm::scale(local, instance.transforms[i].scale);
                
                instance.transforms[i].world_matrix = parent * local;
                
            }
        }
    }

}