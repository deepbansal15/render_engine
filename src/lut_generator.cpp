#include "lut_generator.h"
#include "glad/glad.h"
#include "renderer/glprogram.h"
#include "renderer/shaders.h"

namespace render_engine
{
    namespace lut_generator
    {
        void generate_lut(uint32_t& tex_id)
        {
            GLProgram program;
            program = glprogram::create(shaders::brdf_vert,shaders::brdf_frag,false);

            uint32_t fbo;
            uint32_t rbo;
            glGenFramebuffers(1, &fbo);
            glGenRenderbuffers(1, &rbo);

            uint32_t lut_texture;
            glGenTextures(1, &lut_texture);

            // pre-allocate enough memory for the LUT texture.
            glBindTexture(GL_TEXTURE_2D, lut_texture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);
            // be sure to set wrapping mode to GL_CLAMP_TO_EDGE
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            // then re-configure capture framebuffer object and render screen-space quad with BRDF shader.
            glBindFramebuffer(GL_FRAMEBUFFER, fbo);
            glBindRenderbuffer(GL_RENDERBUFFER, rbo);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lut_texture, 0);

            glViewport(0, 0, 512, 512);
            glprogram::use(program.program);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            render_quad();

            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            glDeleteFramebuffers(1,&fbo);
            glDeleteRenderbuffers(1,&rbo);

            glprogram::destroy(program);

            tex_id = lut_texture;
        }

        void render_quad()
        {
            static unsigned int vao = 0;
            static unsigned int vbo = 0;
            if (vao == 0)
            {
                float vertices[] = {
                    // positions             // texture Coords
                    -1.0f,  1.0f, 0.0f,1.0f, 0.0f, 1.0f,
                    -1.0f, -1.0f, 0.0f,1.0f, 0.0f, 0.0f,
                     1.0f,  1.0f, 0.0f,1.0f, 1.0f, 1.0f,
                     1.0f, -1.0f, 0.0f,1.0f, 1.0f, 0.0f,
                };
                // setup plane VAO
                glGenVertexArrays(1, &vao);
                glGenBuffers(1, &vbo);
                glBindVertexArray(vao);
                glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);
                glEnableVertexAttribArray(0);
                glEnableVertexAttribArray(GLProgram::ATTRIB_POSITION);
                int stride = 6 * sizeof(float);
                glVertexAttribPointer(GLProgram::ATTRIB_POSITION, 4, GL_FLOAT, 
                                      GL_FALSE,stride , (void*)0);
                glEnableVertexAttribArray(GLProgram::ATTRIB_TEX_COORD);
                glVertexAttribPointer(GLProgram::ATTRIB_TEX_COORD, 2, GL_FLOAT, 
                                      GL_FALSE, stride, 
                                      (void*)(4 * sizeof(float)));
            }
            glBindVertexArray(vao);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
            glBindVertexArray(0);
        }
    }


}