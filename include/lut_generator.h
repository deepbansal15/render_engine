#ifndef __LUT_GENERATOR_H__
#define __LUT_GENERATOR_H__

#include "inttypes.h"

namespace render_engine
{
    namespace lut_generator
    {
        void generate_lut(uint32_t& tex_id);
        void render_quad();
    }
}

#endif