/// ------------------  Pre-define section  ------------------ ///

#ifdef __NORMALMAP__
#define _TANGENT 1
#else
#define _TANGENT 0
#endif

#ifdef __DETAILMAP__
#define _UV1 1
#else
#define _UV1 0
#endif

/// Light count is defined by the scene
#define MAX_POINT_LIGHT_COUNT 1
#define MAX_DIRECTIONAL_LIGHT_COUNT 1
#define MAX_SPOTLIGHT_LIGHT_COUNT 1

/// ------------------  Pre-define section end  ------------------ ///

/// ------------------  Attribute section  ------------------ ///

#if (VERTEX_STAGE)

in vec4 a_position;
in vec3 a_normal;

#if _TANGENT
in vec3 a_tangent;
#endif
in vec2 a_uv0;

#if _UV1
in vec2 a_uv1;
#endif

#endif

/// ------------------  Attribute section end  ------------------ ///

/// ------------------  Interface section  ------------------ ///

#if (VERTEX_STAGE)

out v2f_out
{
    vec4 position;
    vec3 normal;
    
    #if (_TANGENT)
    
    vec3 tangent;
    
    #endif
    
    vec2 uv0;
    
    #if (_UV1)
    
    vec2 uv1;
    
    #endif

} vs_out;

#endif

#if (FRAGMENT_STAGE)

in v2f_out
{
    vec4 position;
    vec3 normal;

    #if _TANGENT
    
    vec3 tangent;
    
    #endif
    
    vec2 uv0;
    
    #if _UV1
    
    vec2 uv1;
    
    #endif

} fs_in;

#endif

/// ------------------  Interface section end  ------------------ ///

/// ------------------  Lights section  ------------------ ///

/// structs cannot be used as input/output variables
// struct PointLight
// {
//     vec3 position;
//     vec3 color;
// };

// uniform PointLight point_light[MAX_POINT_LIGHT_COUNT];

// struct DirectionalLight
// {
//     vec3 direction;
//     vec3 color;
// };

// uniform DirectionalLight directional_light[MAX_DIRECTIONAL_LIGHT_COUNT];

// struct SpotLight
// {

// };

// uniform SpotLight spot_light[MAX_DIRECTIONAL_LIGHT_COUNT];

uniform vec3 l_point_light_pos[4];
uniform vec3 l_point_light_color[4];

/// ------------------  Lights section end  ------------------ ///


/// ------------------  Sky section  ------------------ ///



/// ------------------  Sky section end  ------------------ ///


/// ------------------  Global  ------------------ ///

uniform mat4 u_mvp_matrix;
uniform mat4 u_p_matrix;
uniform mat4 u_v_matrix;
uniform mat4 u_m_matrix;
uniform vec3 u_camera_pos;

/// ------------------  Global end  ------------------ ///

