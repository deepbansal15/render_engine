#include <stdio.h>
#include <unistd.h>
#include <iostream>

// Include Window and GL loader
#include "glad/glad.h"
#include <GLFW/glfw3.h>

// Include math library
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_projection.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/io.hpp>

#include "camera.h"
#include "fps_camera.h"

#include "engine_ui/material_editor.h"

#define IMGUI_IMPL_OPENGL_LOADER_GLAD

#include "ui/imgui.h"
#include "ui/imgui_impl_glfw.h"
#include "ui/imgui_impl_opengl3.h"

#include "rtr_demo.h"

render_engine::Camera camera;

int frame_width;
int frame_height;
float  delta;
int last_key;
int last_action;
float last_xpos;
float last_ypos;

glm::vec3 camera_position(0,1.0f,0);

void setup_imgui(GLFWwindow* window)
{
    const char* gl_version = "#version 410";
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window,true);
    ImGui_ImplOpenGL3_Init(gl_version);
}

void get_float3(const glm::vec3& val,float* array)
{
    array[0] = val.x;
    array[1] = val.y;
    array[2] = val.z;
}

void draw_ui()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    /// Do all material modifications here   
    //modify_transform();
    render_engine::demo4_rtr::render_ui();
    
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void shutdown_ui()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void render(const render_engine::Camera& cam)
{
    render_engine::demo4_rtr::render(cam);
    draw_ui();
}

void init()
{
    /// eye + direction = centre
    camera.position = glm::vec3(0,0,10.0f);
    camera.direction = glm::vec3(0,0,-1);
    render_engine::demo4_rtr::init();
}

void updatescene(GLFWwindow* window) {	
    
	// Wait until at least 16ms passed since start of last frame (Effectively caps framerate at ~60fps)
	static double  last_time = 0;  
    // Returns time in seconds
	double  curr_time = glfwGetTime();
	delta = (curr_time - last_time);

	if (delta > 0.03f)
    {
        render_engine::fps_camera::keyboard_input(last_key,last_action,delta,&camera);
        render_engine::fps_camera::mouse_input(last_xpos,last_ypos,delta,&camera);
        glViewport(0,0,frame_width,frame_height);
        render(camera);

        delta = 0.0f;
        last_time = curr_time;
        glfwSwapBuffers(window);
    }
    else
    {
        usleep(5000);
    }
	
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    printf("size changed \n");

    glfwGetFramebufferSize(window,&frame_width,&frame_height);
    
    camera.width = frame_width;
    camera.height = frame_height;
    render_engine::camera::update_projection_matrix(camera);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    last_key = key;
    last_action = action;
    //return;
    glm::vec3 pos(0);
    //transform_instance.transforms[0].rotation = glm::radians(glm::vec3(-90,0,0));
    if(action == GLFW_REPEAT)
    {
        switch (key)
        {
            // back
            case GLFW_KEY_I:
            pos += glm::vec3(0,1,0);
            break;
            case GLFW_KEY_K:
            pos += glm::vec3(0, -1,0);
            break;
            // Up
            case GLFW_KEY_J:
            pos += glm::vec3(-1.0f,0,0);
            break;
            case GLFW_KEY_L:
            pos += glm::vec3(1.0f,0,0);
            break;  
            case GLFW_KEY_M:
            pos += glm::vec3(0.0f,0.0f,1.0f);
            break;
            case GLFW_KEY_N:
            pos -= glm::vec3(0.0f,0.0f,1.0f);
            break;

        }
    }
    //point_light_position[0] = pos;
    //light_pos = pos;
    //printf("%f %f %f\n",pos[0],pos[1],pos[2]);
}

void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
    last_xpos = xpos;
    last_ypos = ypos;
}

void drop_callback(GLFWwindow* window,int count, const char** paths)
{
    int i;
    for(i=0;i<count;i++)
    {
        printf("file path %s \n",paths[i]);
    }
}

int main_gl()
{
    /// Initialize GLFW
    if (!glfwInit())
    {
        // Handle initialization failure
        printf("GLFW cannot be initialized \n");
        return -1;
    }
    
    /// OpenGL 4.1 context is not creating the window
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    /// Create the window
    GLFWwindow* window = glfwCreateWindow(1280, 720, "My Title", NULL, NULL);
   
    /// Check if window is built otherwise terminate
    if(!window)
    {
        printf("Required window cannot be created \n");
        glfwTerminate();
        return -1;
    }
    
    /// Make GL context current to the window
    glfwMakeContextCurrent(window);
    
    /// Load glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        glfwTerminate();
        printf("Failed to initialize GLAD \n");
        return -1;
    }

    if (GLAD_GL_VERSION_4_1)
    {
        printf("Using OpenGL 4.1 version \n");
    }

    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, cursor_pos_callback);
    glfwSetDropCallback(window,drop_callback);

    setup_imgui(window);

    glfwGetFramebufferSize(window,&frame_width,&frame_height);
    camera = render_engine::camera::create_perpective(frame_width,frame_height,45,0.1,1000);
    init();

    /// tell GL to only draw onto a pixel if the shape is closer to the viewer
	
    glEnable (GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc (GL_LESS); // depth-testing interprets a smaller value as "closer"

    /// While user doen't press the close button do not exit
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        glClearColor(0.2,0.2,0.2,1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        updatescene(window);
        
    }

    /// Cleanup
    render_engine::demo4_rtr::shutdown();
    shutdown_ui();

    /// Destroy the window
    glfwDestroyWindow(window);
    /// For terminating the window
    glfwTerminate();
    return 0;
}

int main()
{
    main_gl();
    return 0;
}