#ifndef __SKYBOX_H__
#define __SKYBOX_H__

#include <inttypes.h>
#include <string>
#include <glm/glm.hpp>

#include "texture.h"
#include "renderer/glprogram.h"

namespace render_engine
{
    struct Camera;
    struct Skybox
    {
        enum
        {
            PROCEDURAL,
            HDR,
            CUBEMAP
        };
        
        int type;

        uint32_t vao;
        uint32_t vbo;

        Texture tex;
        SkyboxTexture hdr_tex;
        GLProgram gl_program;
    };

    namespace skybox
    {
        Skybox create_skybox(int type,const char* path);
        void destroy_skybox(Skybox& skybox);
        void render_skybox(const Skybox& skybox,const Camera& cam);
    }
}

#endif