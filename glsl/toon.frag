out vec4 frag_colour;

uniform vec3 c_u_albedo_t;
uniform int c_u_levels = 3;

uniform float c_u_diffuse_ref;

vec3 compute_lighting()
{ 
    float scale_factor = 1.0/c_u_levels;
    /// point light
    vec3 n = normalize(fs_in.normal);
    vec3 s = normalize(l_point_light_pos[0] - fs_in.position.xyz);
    vec3 ambient = 0.5 * c_u_albedo_t;
    float cosine = max(dot(s, n),0.0);
    vec3 diffuse = c_u_diffuse_ref * floor(cosine * c_u_levels) * scale_factor * c_u_albedo_t;

    return diffuse + ambient;

}

void main()
{
	frag_colour = vec4(compute_lighting(),1.0);
}